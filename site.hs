--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import Data.Monoid (mappend)
import Hakyll
import Text.CSL
import Text.CSL.Pandoc
import Text.Pandoc
import qualified Data.Map as M


--------------------------------------------------------------------------------
main :: IO ()
main = hakyllWith config $ do

    match "files/**" $ do
        route   idRoute
        compile copyFileCompiler

    match "bibliography/library.bib" $ do
        route   idRoute
        compile biblioCompiler

    match "bibliography/chicago-syllabus.csl" $ do
        route   idRoute
        compile cslCompiler

    match "bibliography/chicago-author-date.csl" $ do
        route   idRoute
        compile cslCompiler

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match (fromList ["pages/research.md", "pages/personal.md"]) $ do
        route   $ setExtension "html"
        compile $ bibtexCompiler "bibliography/chicago-author-date.csl" "bibliography/library.bib"
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls

    match "pages/cv.md" $ do
        route   $ setExtension "html"
        compile $ pandocBiblioCompiler "bibliography/chicago-syllabus.csl" "bibliography/library.bib"
            >>= loadAndApplyTemplate "templates/cv.html" defaultContext
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls

    match "posts/*/*" $ do
        route $ setExtension "html"
        compile $ bibtexCompiler "bibliography/chicago-author-date.csl" "bibliography/library.bib"
            >>= saveSnapshot "teaser-snapshot"
            >>= loadAndApplyTemplate "templates/post.html"    postCtx
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    create ["pages/blog.html"] $ do
        route idRoute
        compile $ do
            goalposts <- loadAll "posts/goal/*"
            paperposts <- loadAll "posts/papers/*"
            let blogCtx =
                    listField "goal-posts" teaserCtx (return goalposts) `mappend`
                    listField "paper-posts" teaserCtx (return paperposts) `mappend`
                    constField "title" "Blog"                  `mappend`
                    defaultContext
            makeItem ""
                >>= loadAndApplyTemplate "templates/blog.html" blogCtx
                >>= loadAndApplyTemplate "templates/default.html" blogCtx
                >>= relativizeUrls


    match "index.html" $ do
        route idRoute
        compile $ do
            let indexCtx =
                    constField "title" "Home" `mappend`
                    defaultContext

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler


--------------------------------------------------------------------------------


postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext

teaserCtx :: Context String
teaserCtx = teaserField "teaser" "teaser-snapshot" `mappend` postCtx

config :: Configuration
config = defaultConfiguration { destinationDirectory = "public" }

addLinkCitations :: Pandoc -> Pandoc
addLinkCitations (Pandoc meta a) =
  let prevMap = unMeta meta
      newMap = M.insert "link-citations" (MetaBool True) prevMap
      newMeta = Meta newMap
  in  Pandoc newMeta a

myReadPandocBiblio :: ReaderOptions
                   -> Item CSL
                   -> Item Biblio
                   -> Item String
                   -> Compiler (Item Pandoc)
myReadPandocBiblio ropt csl bbl itm = do
    -- Parse CSL file, if given
    style <- unsafeCompiler $ readCSLFile Nothing . toFilePath . itemIdentifier $ csl

    -- We need to know the citation keys, add then *before* actually parsing the
    -- actual page. If we don't do this, pandoc won't even consider them
    -- citations!
    let Biblio refs = itemBody bbl
    pandoc <- itemBody <$> readPandocWith ropt itm
    let pandoc' = processCites style refs (addLinkCitations pandoc) -- here's the change

    return $ fmap (const pandoc') itm

bibtexCompilerWith
    :: ReaderOptions
    -> WriterOptions
    -> Identifier
    -> FilePath
    -> Compiler (Item String)
bibtexCompilerWith readerOpts writerOpts cslPath bibPath = do
    csl <- load cslPath
    bib <- load (fromFilePath bibPath)
    writePandocWith writerOpts <$> (getResourceBody >>= myReadPandocBiblio readerOpts csl bib)

bibtexCompiler
    :: Identifier
    -> FilePath
    -> Compiler (Item String)
bibtexCompiler =
    let rdr = defaultHakyllReaderOptions
        wrtr = defaultHakyllWriterOptions
            { writerHTMLMathMethod = MathJax "" }
     in bibtexCompilerWith rdr wrtr
