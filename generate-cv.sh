#!/bin/bash
stack exec pandoc -- -t html5 --template templates/cv-pdf.html -s --bibliography bibliography/library.bib --csl bibliography/chicago-syllabus.csl --css css/default.css pages/cv.md -V margin-top=13 -V margin-left=3 -V margin-right=5 -V margin-bottom=13 -o files/sacha-sokoloski.pdf --pdf-engine-opt=--enable-local-file-access
