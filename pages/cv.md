---
title: CV
author: Sacha Sokoloski
---

<style>

table {
    width: 100%;
    page-break-inside: avoid;
}

ul {
    margin-top: 6px;
}

h1 {
    margin-top: 30px;
}

.keep-together {
    page-break-inside: avoid;
}
</style>

<div class="keep-together">
# Education

PhD in Informatics                                                June 2013 to May 2019
---------------------------------------------------------   ---------------------------
Max Planck Institute for Mathematics in the Sciences                 Leipzig, Germany

- **Supervisor:** Professor Nihat Ay
- **Dissertation Submitted:** October 2017
- **Dissertation Defended:** May 2019
- **Paternity leave**: During my PhD I had my first and second child and took periods of leave for both.
</div>

Master's in Computational Neuroscience                          October 2010 to May 2013
----------------------------------------------------------   ---------------------------
Bernstein Centre for Computational Neuroscience Berlin                 Berlin, Germany

- **Supervisor:** Professor Manfred Opper


Bachelor of Science                             May 2004 to May 2009
-----------------------------------       --------------------------
University of Toronto                              Toronto, Canada

- **Specialist:** Cognitive Science and Artificial Intelligence
- **Major:** Philosophy
- **Minor:** Statistics

<div class="keep-together">
# Bibliography

### Peer-Reviewed

- [@franke_asymmetric_2024] [[Link]](https://elifesciences.org/articles/89996){class="hideable"}
- [@sokoloski_modelling_2021] [[Link]](https://elifesciences.org/articles/64615){class="hideable"} [[PDF]](/files/pdfs/sokoloski_modelling_2021.pdf){class="hideable"}
- [@sokoloski_implementing_2017] [[Link]](http://dx.doi.org/10.1162/neco_a_00991){class="hideable"} [[PDF]](/files/pdfs/sokoloski_implementing_2017.pdf){class="hideable"}
</div>

### Preprints

- [@sokoloski_unified_2024] [[Link]](https://arxiv.org/abs/2404.19501){class="hideable"} [[PDF]](/files/pdfs/sokoloski_unified_2024.pdf){class="hideable"}
- [@sokoloski_computational_2024] [[Link]](https://arxiv.org/abs/2402.05266){class="hideable"} [[PDF]](/files/pdfs/sokoloski_computational_2024.pdf){class="hideable"}
- [@schmors_effects_2023] [[Link]](https://www.biorxiv.org/content/10.1101/2023.10.18.562960v1){class="hideable"}
- [@sokoloski_hierarchical_2022] [[Link]](https://arxiv.org/abs/2206.04841){class="hideable"} [[PDF]](/files/pdfs/sokoloski_hierarchical_2022.pdf){class="hideable"}
- [@sokoloski_biologically_2012] [[Link]](http://arxiv.org/abs/1210.4145){class="hideable"} [[PDF]](/files/pdfs/sokoloski_biologically_2012.pdf){class="hideable"}

### Theses

- [@sokoloski_implementing_2019] [[Link]](http://nbn-resolving.de/urn:nbn:de:bsz:15-qucosa2-347034){class="hideable"} [[PDF]](/files/pdfs/sokoloski_implementing_2019.pdf){class="hideable"}
- [@sokoloski_efficient_2013] [[PDF]](/files/pdfs/sokoloski_efficient_2013.pdf){class="hideable"}

# Professional Activities

### Conferences and Workshops

International Convention on the Mathematics of Neuroscience and AI                May 2024
-------------------------------------------------------------------  ---------------------
EMBL Heidelberg                                                                Rome, Italy

- **Talk & Poster:**  Analytically-tractable hierarchical models for neural data analysis and normative modelling *(Sacha Sokoloski, Philipp Berens)*



EMBO Workshop: Subcortical sensory circuits                                November 2023
---------------------------------------------                      ---------------------
EMBL Heidelberg                                                      Heidelberg, Germany

- **Poster:**  A computational approach to visual ecology with deep reinforcement learning *(Sacha Sokoloski, Jure Majnik, Philipp Berens)*
- Received best poster award


European Retina Meeting                                                   September 2023
---------------------------------------------                      ---------------------
University of Tübingen                                                 Tübingen, Germany

- **Talk:**  Modelling ecological constraints on visual processing with deep reinforcement learning *(Sacha Sokoloski, Jure Majnik, Thomas Euler, Philipp Berens)*


Cosyne                                     March 2023
-----------------------           -------------------
Fairmont The Queen Elizabeth         Montreal, Canada

- **Poster:**  Modelling ecological constraints on visual processing with deep reinforcement learning *(Sacha Sokoloski, Jure Majnik, Thomas Euler, Philipp Berens)*
- Received travel grant


Population Models Workshop                     July 2022
------------------------------     ---------------------
University of Edinburgh              Edinburgh, Scotland

- **Invited Talk:**  Exact learning and inference in large-scale probabilistic graphical models

Cosyne                               March 2022
-----------------------     -------------------
Lisboa Congress Centre         Lisbon, Portugal

- **Poster:**  Disentangling neural dynamics with fluctuating hidden Markov models (*Sacha Sokoloski, Ruben Coen-Cagli*)
- **Poster:**  An interpretable spline-LNP model to characterize feedforward and feedback processing in mouse dLGN (*Lisa Schmors, Yannik Bauer, Ziwei Huang, Lukas Meyerolbersleben, Simon Renner, Ann H. Kotkat, Davide Crombie, Sacha Sokoloski, Laura Busse, Philipp Berens*)

Cosyne                            February 2021
-----------------------     -------------------
Online

- **Poster:**  Information-limiting correlations linearize the neural code (*Sacha Sokoloski, Ruben Coen-Cagli*)
- **Poster:**   Neural sampling from bimodal distributions in primary visual cortex (*Ruben Coen-Cagli, Adam Kohn, Sacha Sokoloski*)

Cosyne                            February 2020
-----------------------     -------------------
Hilton Denver City Center           Denver, USA

- **Poster:** Mixture of poisson models for neural correlations and coding (*Sacha Sokoloski, Ruben Coen-Cagli*)
- Received travel grant

Sense2Synapse                     April 2017
-----------------------  -------------------
Rockefeller University    New York City, USA

- **Poster:** Dynamic Bayesian Inference in the Brain

International Conference on Mathematical Neuroscience                    May 2016
-------------------------------------------------------   -----------------------
Antibes Juan-les-Pins Conference Centre                     Juan-les-Pins, France

- **Talk:** Learning to Filter Spike Trains when the Stimulus Dynamics are Unknown and Nonlinear

HaL-10: Haskell in Leipzig            December 2015
----------------------------    -------------------
HTWK Leipzig                       Leipzig, Germany

- **Talk:** Typsichere Numerische Optimierung basierend auf Mannigfaltigkeiten
- **English title:**  Typesafe Numerical Optimization based on Manifolds

### Teaching

Teaching Essential Statistics                    Winter 2022/23 & Winter 2023/24
---------------------------------------         --------------------------------
University of Tübingen

- Teaching the Master's course "Essential Statistics for Neuroscience".

### Mentorships

PhD Student: Fabio Seel                                    September 2023 to Present
---------------------------------------                --------------------
Hertie Institute for AI in Brain Health

- Co-supervising [Fabio Seel](https://hertie.ai/data-science/team/members/fabio-seel) with Philipp Berens in the area of retinal
  modelling with deep neural networks.


Master's Student: Jure Majnik                             Winter 2021/22 & Fall 2022
---------------------------------------                --------------------
University of Tübingen

- Supervised [Jure Majnik](https://github.com/juremaj) on developing code for the [retinal-rl](retinal rlhttps://github.com/berenslab/retinal-rl) project.

Google Summer of Code: Zarak Mahmud                             Summer 2022
---------------------------------------                --------------------
Haskell.org

- Mentored [Zarak Mahmud](https://gitlab.com/qzarak) through the Google Summer of Code program to add GPU computation to my [Geometric Optimization Libraries](https://gitlab.com/sacha-sokoloski/goal).


Google Summer of Code: Andrew Knapp                             Summer 2018
---------------------------------------                --------------------
Haskell.org

- Mentored a student through the Google Summer of Code program on developing a library for parallel automatic differentiation for the Haskell.org organization.
- Project was overall [successful](https://summer.haskell.org/news/2018-09-01-final-results.html).

### Peer Review

- PLOS Computational Biology (2024)
- IEEE Transactions on Artificial Intelligence (2023)
- PLOS Computational Biology (2023)
- Neural Processing Letters (2022)
- Nature Neuroscience (Co-review, 2022)
- eLife (2021)
- Nature Communications (Co-review, 2019)
- IEEE Transactions on Neural Networks and Learning Systems (2017)


# Employment History

Group Leader                                        January 2024 to Present
-------------------------------------------     ---------------------------
Hertie Institute for AI in Brain Health                    Tübingen, Germany

- **Group:** Neuronal Modelling
- **Director:** Philipp Berens

Postdoctoral Researcher                           July 2021 to December 2023
-------------------------------------------     ---------------------------
University of Tübingen                                  Tübingen, Germany

- **Principal Investigator:** Philipp Berens
- **Project:** Modelling neural circuits in the retina

Research Fellow                                     October 2019 to April 2021
-------------------------------------------     ---------------------------
Albert Einstein College of Medicine                         New York, USA

- **Principal Investigator:** Ruben Coen-Cagli
- **Project:** Modelling Bayesian inference in neural circuits and analyzing neural population response recordings

Research Trainee                                    November 2017 to September 2019
-------------------------------------------     -----------------------------------
Albert Einstein College of Medicine                                 New York, USA

- **Principal Investigator:** Ruben Coen-Cagli

<!--
### Professional

### Student

Research Assistant            May 2012 to July 2012
----------------------   --------------------------
University of Geneva            Geneva, Switzerland

- **Principal Investigator:** Professor Alexandre Pouget
- **Project:** Modelling sacaddic eye control using probabilistic population codes

Programmer                          August 2011 to May 2013
------------------------------   --------------------------
Technical Unversity of Berlin               Berlin, Germany

- **Principal Investigator:** Marianne Maertens
- **Project:** Developing software in Python to control scientific hardware for visual psychophysics

Programmer                                  November 2009 to July 2010
-----------------------------------      -----------------------------
University of British Columbia                       Vancouver, Canada

- **Principal Investigator:** Professor Kurt Haas
- **Project:** Developing software in C to control a fast 2-photon microscope

Research Assistant                  April 2009 to July 2009
---------------------------      --------------------------
University of Toronto                       Toronto, Canada

- **Principal Investigator:** Jun Luo
- **Project:** Writing an annotated bibliography in the Philosophy of Information & Computation

Programmer                       June 2008 to December 2008
---------------------------    ----------------------------
University of Toronto                       Toronto, Canada

- **Principal Investigator:** Professor Gerald Penn
- **Project:** Developing software in Prolog to estimate probability distributions over parsed linguistic corpora

Research Assistant               June 2008 to December 2008
---------------------------    ----------------------------
University of Toronto                       Toronto, Canada

- **Principal Investigator:** Jun Luo
- **Project:** Coordinating the "Synesthetes" faculty meeting group, focusing on cognitive science and interdisciplinary research

Programmer                     December 2007 to August 2008
---------------------------  ------------------------------
University of Toronto                       Toronto, Canada

- **Principal Investigator:** Professor Fahiem Bacchus
- **Project:** Developing software in Java to schedule meeting times of potential graduate students

# Miscellaneous

### Summer/Winter Schools

Dynamics of Multi-Level Systems                                     Summer 2015
---------------------------------------------------------   -------------------
Max Planck Institute for the Physics of Complex Systems        Dresden, Germany

Autonomous Learning                                             Fall 2014
------------------------------------------------------ ------------------
Max Planck Institute for Mathematics in the Sciences     Leipzig, Germany

Deep Learning and Feature Learning                                    Summer 2012
-------------------------------------------------    ----------------------------
Institute for Pure and Applied Mathematics UCLA        Los Angeles, United States

The Future of the Embodied Mind                                                    Fall 2011
--------------------------------------------------------------------   ---------------------
European Project: Extending Sensorimotor Contingencies to Cognition     San Sebastian, Spain

Advanced Numerical Bifurcation Analysis                                     Summer 2011
----------------------------------------------------------    -------------------------
Danish Centre for Applied Mathematics and Mechanics                 Copenhagen, Denmark

<div class="keep-together">
### Scholarships and Awards

----------------------------------------------------------          ------------------
**Network of European Neuroscience Schools**                           **Summer 2012**
Scholarship for training stay in lab of Alexandre Pouget
**Sunflower Scholarship in Philosophy**                                **Spring 2007**
Award for undergraduate writing in philosophy
----------------------------------------------------------          ------------------
</div>


### Notable Skills
-->

# Miscellaneous

#### Languages
- English (Mother tongue)
- German (Fluent, C1)
- French (Working knowledge)

#### Computer Skills

- Programming Languages: Haskell, C, Python, Matlab, R, Java, Prolog
- Advanced Linux user
- Experience maintaining a software library with approximately 10,000 lines of code
- Experience writing software for interfacing with scientific hardware
