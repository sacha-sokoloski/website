---
title: Research
---

<style>

figure.code {
    float: right;
    margin-top: 20px;
    max-width: 180px;
    min-width: 180px;
    background-color: #232629;


@media (max-width: 559px) {

    img.hgm {
        height: auto;
        width: 260px;
        margin: auto;
        padding: 6px;
    }

    img.log-likelihood {
        width: 95%;
        height: auto;
        padding: 2.5%;
    }

    a.kant {
        height: auto;
        width: 200px;
        margin: auto;
    }

    img.kant {
        height: auto;
        width: 220px;
    }

}


}

@media (min-width: 560px) {

    figure.brain {
        width: 39%;
        margin-top: 5px;
    }

    figure.hgm {
        margin-top: 20px;
        width: 34%;
    }

    figure.log-likelihood {
        width: 48%;
        margin-top: 8px;
    }

    figure.kant {
        width: 28%;
        margin-top: 12px;
        margin-bottom: 6px;
    }

}

</style>

*(NB: This page needs a round of updating, though remains relevant. I still like Kant).*

<figure class="brain float left-float">
<a title="Selket [CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0/)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Ventral-dorsal_streams.svg">
<img class="brain float left-float" alt="Ventral-dorsal streams" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Ventral-dorsal_streams.svg/512px-Ventral-dorsal_streams.svg.png"></a>
<figcaption class="float"> Neurons in visual cortex (blue) compute simple features of the visual input. Their output is sent along the dorsal (green) and ventral (purple) streams to compute object location and identity. </figcaption>
</figure>

### Towards a Theory of Neural Computation

I aim to understand how populations of neurons implement elementary computations, and how these computations are combined to support complex cognition. Towards this aim, I develop theories of how the brain infers the state of the world and refines its inferences through experience, in a manner that is consistent with the distributed, recurrent, and hierarchical nature of neural computation. In order to validate theories of the brain, I develop statistical models that capture hidden structure in noisy neural recordings, and computer libraries capable of simulating and fitting such models efficiently and without error. In the following paragraphs I will sketch the past, present, and future of my work in these areas.

<figure class="hgm float right-float">
<img class="hgm float right-float" src="/files/random-images/hgm.png" alt="Hierarchical Model of the World">
<figcaption class="float">The brain as a hierarchical graphical model: the bottom of the hierarchy (Z) are sensations over time (t). Higher-level properties and concepts (Y and X) are dynamically inferred by the brain based on the sensations.</figcaption>
</figure>

### The Bayesian Brain

I take the view that the brain encodes a recurrent, hierarchical model of the world [@friston_hierarchical_2008; @pitkow_inference_2017]. In this view, sensory receptors (e.g.\ the retina) constitute the bottom of the hierarchy, and the brain infers encodings of higher-level properties and concepts given the dynamic activity of its senses. Bayesian inference is the mathematically rigorous formulation of this inference problem [@pouget_probabilistic_2013], but solving Bayesian inference problems is rarely trivial.

In @sokoloski_implementing_2017 I showed how this problem can be solved exactly for a simple form of theoretical, dynamic neural population when the parameters of the neural population satisfy certain constraints. The results in this paper were a subset of the work I did in my dissertation [@sokoloski_implementing_2019], wherein I analyzed the general conditions under which dynamic Bayesian inference is computationally tractable.

Ultimately, animals infer the state of the world in order to behave effectively, and in my master's thesis I did preliminary work on formulating motor control as a tractable inference problem [@sokoloski_efficient_2013]. As a postdoctoral researcher in the lab of [Philipp Berens](https://summer.haskell.org/news/2018-09-01-final-results.html), I am combining my experience in control theory and neural modelling to understand how reinforcement learning drives cellular diversity and the functional architecture of the retina.

### Neural and Machine Learning

Bayesian inference combines information from observations with prior knowledge. In the Bayesian framework, the question of how the brain learns reduces to how the brain refines its prior knowledge given its history of sensory experience [@berkes_spontaneous_2011]. This reduction thus unifies the problem of how the brain learns with the problem of how to fit models to neural data; both problems can be solved by the method of maximum likelihood.

<figure class="log-likelihood float left-float">
<img class="log-likelihood float left-float" src="/files/random-images/log-likelihood-ascent.png" alt="Novel Training Algorithms">
<figcaption class="float">Stochastic expectation-maximization (SEM) and stochastic gradient ascent (SGA) can fit models of correlated neural activity. With a bit of math (Hybrid) we may exceed them and achieve near optimal performance [@sokoloski_conditional_2019].</figcaption>
</figure>

In the lab of <a href="https://sites.google.com/site/rubencoencagli/home">Ruben Coen-Cagli</a> I developed a novel model of correlated, context-dependent neural population activity, and an efficient algorithm for fitting it to data [@sokoloski_modelling_2021]. Neural correlations can have a profound effect on neural computation [@kohn_correlations_2016], and one of my ongoing projects is to understand if the correlations amongst neurons in primary visual cortex might facilitate tractable Bayesian inference.

In my dissertation [@sokoloski_implementing_2019] I did preliminary work on a novel algorithm for fitting hierarchical models, which remains an open problem in machine learning and computational neuroscience [@lecun_deep_2015]. This algorithm relies on the aforementioned constraints for tractable Bayesian inference, and the work I have so far described constitute steps towards implementing it. If I am successful in realizing this larger project, then I hope to demonstrate how the organization of tuning and correlations in neural populations facilitate both tractable inference and learning in the brain.

<figure class="right-float float code">
``` haskell
 (<.>)
   :: c # x
   -> c #* x
   -> Double
```
<figcaption class="float">Dot product of two points in Goal. The first point is in **c** coordinates on the **x** manifold, and the second is in the dual space of the first.</figcaption>
</figure>

### Type-Safe Numerical Optimization

In attempting to implement, apply, and simulate complex models, one comes up against human limitations in our ability to write error-free machine code. Strongly-typed languages assist us in avoiding such errors by granting the compiler the ability to verify our code before we run it. At the same time, one cannot embed all code at the type-level, and libraries written in strongly-typed languages must tradeoff verifiability with practicality.

I have developed a set of libraries for the programming language [Haskell](https://www.haskell.org/) which I call the Geometric Optimization Libraries (Goal). Goal provides types and functions based on simple ideas from differential and information geometry [@amari_methods_2007]. Essentially, Goal distinguishes vectors of numbers as points on a manifold in a given coordinate system. Many fundamental mathematical operations can be formulated as changes in coordinates, and Goal thereby provides a compact yet general interface to optimization and statistics. Moreover, by embedding these fundamental concepts at the type-level, Goal allows Haskell type-checkers to serve as simple proof assistants, and in this way Goal facilitates my mathematical work as well.

<figure class="kant float left-float">
<a class ="kant float left-float" title="Stéphane Lemarchand Caricaturiste [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Caricature_Kant_2007.jpg">
<img class="kant float left-float" alt="Caricature Kant 2007" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Caricature_Kant_2007.jpg/256px-Caricature_Kant_2007.jpg"></a>
<figcaption class="float">Immanuel Kant showed how we may arrive at truths about reality through reason alone.</figcaption>
</figure>

### Embodiment, Enaction, and Transcendental Arguments

I believe that data cannot speak for itself, and must be interpeted through the lens of well-motivated theory. Physicists have produced insights in many fields by applying their theories and physical intuition to revealing simple structures in data. Nevertheless, a purely physicalist view of the brain ignores what our faculty of introspection and our innate intuitions as cognitive beings reveal about the structure of cognition and neural activity. In particular, I believe that theories of the brain as a Bayesian inference machine are underconstrained [@bowers_bayesian_2012;@colombo_bayes_2012], and that we may at least partially address this with *a priori* analyses of the embodied and enactive nature of cognition [@thompson_mind_2007;@stewart_enaction_2011].

Once upon a time I was a student of philosophy. Although I did not pursue a career in the subject, philosophy continues to inform my research. I dream of one day teaching introductory philosophy courses, and perhaps even contributing to the field in my winter years.

<br>

# Bibliography
