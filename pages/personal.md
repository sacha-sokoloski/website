---
title: Personal
---

<style>

@media (max-width: 799px) {

    img  {
        width: 45%;
        float: right;
        object-fit: cover;
        margin: 2.5%;
    }

    figure  {
        min-width=100%;
        max-width=100%;
        margin: auto;
    }
}

@media (min-width: 800px) {

    img  {
        width: 250px;
        height: 480px;
        float: right;
        padding-left: 2%;
        margin-top: 10px;
        margin-bottom: 20px;
        margin-right: 0px;
        object-fit: cover;
    }

    figure  {
        min-width=100%;
        max-width=100%;
        margin: 0px;
    }
}

</style>


I was born in Toronto, Canada on the birthday of Johannes Kepler. I currently live in Heidelberg, Germany with my daughter Evelin (on arm), son Nikolai (on head), and wife Anna (feral). Anna is also a scientist, and we collaborate on solving the two-body problem.

<figure>
<img src="/files/photos/family1.jpg" alt="Cafe in Manhattan" style="object-position: 50% 100%">
<img src="/files/photos/family2.jpg" alt="Leipzig Hauptbahnhof" style="object-position: 50% 0%">
</figure>

In my spare time I enjoy:

- Long walks
- Philosophy
- Video games

Sometimes I'm sporty and I:

- Ride bicycles
- Play tennis
- Paddle canoes

Contrary to popular expectation I do not enjoy:

- Mangos
- Tacos
- Beaches

With all that being said, these days I mostly just dad.
