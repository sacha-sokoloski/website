<!doctype html>

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js">
</script>


<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sacha Sokoloski - Goal Tutorials</title>
        <link rel="stylesheet" href="../../css/default.css" />
        <link rel="stylesheet" href="../../css/syntax.css" />
        <link rel="stylesheet" href="../../css/float.css" />
    </head>
    <body>
        <header>
            <div class="title">
                <a href="../../">Sacha Sokoloski - Goal Tutorials</a>
            </div>
            <nav>
                <a href="../../">Home</a>
                <a href="../../pages/research.html">Research</a>
                <a href="../../pages/blog.html">Blog</a>
                <a href="../../pages/personal.html">Personal</a>
                <a href="../../pages/cv.html">CV</a>
            </nav>
        </header>

        <main role="main">
            <article>
    <section class="header">
        <h2>Introduction to gradient pursuit on manifolds</h2>
        
    </section>
    <section>
        <style>


@media (max-width: 559px) {

    img.descent {
        width: 95%;
        height: auto;
        padding: 2.5%;
    }

}

@media (min-width: 560px) {

    figure.descent {
        width: 50%;
        margin-top: 8px;
    }
}

</style>
<p>In this post we’ll go through the <a href="https://gitlab.com/sacha-sokoloski/goal/tree/master/scripts/gradient-descent">gradient-descent</a> script, and see how to find the maximum of a function with gradient descent in Goal. This post won’t demonstrate why Goal is worth using, but it will serve to introduce many of the basic types and functions which constitute the Goal libraries. Moreover, we’ll see that relatively straightforward things remain relatively straightforward in Goal.</p>
<!--more-->
<p>To begin, let us start with the pragmas</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true"></a><span class="ot">{-# LANGUAGE DataKinds,TypeOperators #-}</span></span></code></pre></div>
<p>and imports</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true"></a><span class="kw">import</span> <span class="dt">Goal.Core</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true"></a><span class="kw">import</span> <span class="dt">Goal.Geometry</span></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true"></a></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true"></a><span class="kw">import</span> <span class="kw">qualified</span> <span class="dt">Goal.Core.Vector.Boxed</span> <span class="kw">as</span> <span class="dt">B</span></span></code></pre></div>
<p>at the beginning of <a href="https://gitlab.com/sacha-sokoloski/goal/tree/master/scripts/gradient-descent/gradient-descent.hs">gradient-descent.hs</a>. This structure for the imports is common to nearly any script and module written with Goal. Firstly, <code>Goal.Core</code> and <code>Goal.Geometry</code> import the essential definitions of the <code>goal-core</code> and <code>goal-geometry</code> packages. In general, each package in the Goal libraries is fully imported with a single import statement, and is designed to be imported unqualified. The only exceptions to this are the vector modules in <code>goal-core</code>. In this case do a qualified import of the <code>Goal.Core.Vector.Boxed</code> module, which re-exports boxed, static-sized vectors (from the <a href="https://hackage.haskell.org/package/vector-sized">vector-sized</a> package), with a few extra definitions for the numeric applications of Goal.</p>
<p>For the sake of efficiency, most gradients in Goal are calculated through a combination of hand-written derivatives, combinators, and class instances. Nevertheless when speed isn’t critical, Goal also provides a simple interface for automatic differentation, as provided by <a href="https://hackage.haskell.org/package/ad">ad</a>. It’s a little ad-hoc, but it does the trick.</p>
<p>So to begin, we want to descend the gradient of the function <code>f</code></p>
<div class="sourceCode" id="cb3"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true"></a><span class="ot">f ::</span> <span class="dt">Floating</span> x <span class="ot">=&gt;</span> <span class="dt">B.Vector</span> <span class="dv">2</span> x <span class="ot">-&gt;</span> x</span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true"></a>f xs <span class="ot">=</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true"></a>    <span class="kw">let</span> (x,y) <span class="ot">=</span> B.toPair xs</span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true"></a>     <span class="kw">in</span> square x <span class="op">+</span> square y <span class="op">+</span> square (x<span class="op">-</span>y)</span></code></pre></div>
<p>which takes a two-dimensional vector as input. In Goal we evaluate functions on manifolds, but in this case since we don’t have any particular geometric structure in mind, we assume that our space of interest is flat and Euclidean. For such cases we have <code>Euclidean</code> manifolds in <code>Cartesian</code> coordinates. To implement gradient descent of <code>f</code> on a Euclidean manifold, we first create the initial point</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true"></a><span class="ot">p0 ::</span> <span class="dt">Cartesian</span> <span class="op">#</span> <span class="dt">Euclidean</span> <span class="dv">2</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true"></a>p0 <span class="ot">=</span> fromTuple (<span class="op">-</span><span class="dv">4</span>,<span class="dv">2</span>)</span></code></pre></div>
<p>the type operator <code>#</code> is an infix synonym for a <code>Point</code>, in this case the point <code>(-4,2)</code> in <code>Cartesian</code> coordinates on the <code>Euclidean</code> plane.</p>
<p>Gradient descent is an iterative optimization algorithm, and we can specify the number of iterations directly, or with some condition. In this example we define the <code>cauchify</code> function, which takes all the elements of a given list until the step-size of an iteration is less than <code>bnd</code>.</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true"></a><span class="ot">bnd ::</span> <span class="dt">Double</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true"></a>bnd <span class="ot">=</span> <span class="fl">0.0001</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true"></a></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true"></a><span class="ot">cauchify ::</span> [<span class="dt">Cartesian</span> <span class="op">#</span> <span class="dt">Euclidean</span> <span class="dv">2</span>] <span class="ot">-&gt;</span> [<span class="dt">Cartesian</span> <span class="op">#</span> <span class="dt">Euclidean</span> <span class="dv">2</span>]</span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true"></a>cauchify <span class="ot">=</span> cauchySequence euclideanDistance bnd</span></code></pre></div>
<p>We then create a gradient pursuit <code>path</code> by appling the <code>cauchify</code> function to the <code>gradientSequence</code> function, which takes the <code>differential</code> of <code>f</code>, a step size <code>eps</code>, a gradient pursuit algorithm <code>gp</code>, and an initial point <code>p0</code>, and returns an infinite list of gradient pursuit steps.</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true"></a><span class="ot">eps ::</span> <span class="dt">Double</span></span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true"></a>eps <span class="ot">=</span> <span class="op">-</span><span class="fl">0.05</span></span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true"></a></span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true"></a><span class="ot">path ::</span> <span class="dt">GradientPursuit</span> <span class="ot">-&gt;</span> [<span class="dt">Cartesian</span> <span class="op">#</span> <span class="dt">Euclidean</span> <span class="dv">2</span>]</span>
<span id="cb6-5"><a href="#cb6-5" aria-hidden="true"></a>path gp <span class="ot">=</span> cauchify <span class="op">$</span> gradientSequence (differential f) eps gp p0</span></code></pre></div>
<p>In the definition of <code>path</code>, we’ve already defined all the arguments to the gradientSequence function except for the desired <code>GradientPursuit</code> algorithm <code>gp</code>. In Goal I’ve implemented three forms of gradient pursuit: classic gradient pursuit, gradient pursuit with momentum, and finally the Adam optimizer.</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true"></a><span class="ot">mtm ::</span> <span class="dt">Double</span></span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true"></a>mtm <span class="ot">=</span> <span class="fl">0.9</span></span>
<span id="cb7-3"><a href="#cb7-3" aria-hidden="true"></a></span>
<span id="cb7-4"><a href="#cb7-4" aria-hidden="true"></a>grds,mtms,<span class="ot">adms ::</span> [<span class="dt">Cartesian</span> <span class="op">#</span> <span class="dt">Euclidean</span> <span class="dv">2</span>]</span>
<span id="cb7-5"><a href="#cb7-5" aria-hidden="true"></a>grds <span class="ot">=</span> path <span class="dt">Classic</span></span>
<span id="cb7-6"><a href="#cb7-6" aria-hidden="true"></a>mtms <span class="ot">=</span> path <span class="op">$</span> defaultMomentumPursuit mtm</span>
<span id="cb7-7"><a href="#cb7-7" aria-hidden="true"></a>adms <span class="ot">=</span> path defaultAdamPursuit</span></code></pre></div>
<figure class="descent float right-float">
<img class="descent float right-float" src="../../files/posts/goal/gradient-descent/gradient-descent.png" alt="Simple Gradient Descent">
</figure>
<p>This is the core of the <a href="https://gitlab.com/sacha-sokoloski/goal/tree/master/scripts/gradient-descent">gradient-descent</a> script, with the rest being definitions for generating plotting data, and feeding the results to <code>gnuplot</code>. On the right we see paths generated by the three gradient pursuit algorithms. Interestingly, although the momentum algorithm is faster than classic gradient pursuit, the Adam optimizer appears quite a bit slower, in spite of being one of the most widely applied gradient optimization algorithms. Nevertheless, in most non-trivial applications Adam is much more efficient than either classic or momentum-based gradient descent, and I personally use it as my standard gradient pursuit algorithm.</p>
    </section>
</article>

        </main>

        <footer>
            <p>email: <a href="mailto:sacha.sokoloski@mailbox.org">sacha.sokoloski@mailbox.org</a></p>
            <p>gitlab: <a href="https://gitlab.com/sacha-sokoloski">gitlab.com/sacha-sokoloski</a> </p>
            <p>Site generated by <a href="http://jaspervdj.be/hakyll">Hakyll</a></p>
        </footer>
    </body>
</html>
