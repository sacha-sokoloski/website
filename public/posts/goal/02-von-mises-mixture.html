<!doctype html>

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js">
</script>


<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sacha Sokoloski - Goal Tutorials</title>
        <link rel="stylesheet" href="../../css/default.css" />
        <link rel="stylesheet" href="../../css/syntax.css" />
        <link rel="stylesheet" href="../../css/float.css" />
    </head>
    <body>
        <header>
            <div class="title">
                <a href="../../">Sacha Sokoloski - Goal Tutorials</a>
            </div>
            <nav>
                <a href="../../">Home</a>
                <a href="../../pages/research.html">Research</a>
                <a href="../../pages/blog.html">Blog</a>
                <a href="../../pages/personal.html">Personal</a>
                <a href="../../pages/cv.html">CV</a>
            </nav>
        </header>

        <main role="main">
            <article>
    <section class="header">
        <h2>Fitting a mixture of von Mises distributions</h2>
        
    </section>
    <section>
        <style>

img  {
    width: 49%;
    height: auto;
    object-fit: cover;
}

figure  {
    margin: 1px;
    border: 0.1em grey solid;
}
</style>
<p>In this post we’ll go through the <a href="https://gitlab.com/sacha-sokoloski/goal/tree/master/scripts/von-mises-mixture">von-mises-mixture</a> script, and see how to fit a mixture of von Mises distributions with Goal. I have developed the Goal libraries primarily to do type-safe statistical modelling, and this post will touch on many of the features of Goal that make it worth using.</p>
<!--more-->
<p>The imports</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true"></a><span class="kw">import</span> <span class="dt">Goal.Core</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true"></a><span class="kw">import</span> <span class="dt">Goal.Geometry</span></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true"></a><span class="kw">import</span> <span class="dt">Goal.Probability</span></span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true"></a><span class="kw">import</span> <span class="dt">Goal.Graphical</span></span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true"></a></span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true"></a><span class="kw">import</span> <span class="kw">qualified</span> <span class="dt">Goal.Core.Vector.Storable</span> <span class="kw">as</span> <span class="dt">S</span></span></code></pre></div>
<p>are standard for a Goal script. The four unqualified imports pull in all the Goal libraries, and the qualified import of <code>Goal.Core.Vector.Storable</code>, provides the tools for working with Vectors that have not been wrapped up as <code>Point</code>s.</p>
<p>Goal treats the task of fitting a model to data as that of finding a <code>Point</code> on a <code>Statistical</code> <code>Manifold</code>. A statistical manifold is a manifold where points are probability distributions. In this case we consider manifolds of Von Mises distributions, which are approximately like normal distributions on a unit circle. The von Mises manifold in Goal is indicated by the <code>VonMises</code> type, and is typically parameterized by two coordinates: the mean and concentration (approximately the inverse variance). In Goal we use the so-called <code>Source</code> coordinates to describe a model in its “Standard” coordinate system, e.g. for normal distributions this is the mean and variance. For von Mises distributions this is the mean and concentration, and we can create a von Mises distribution in source coordinates by writing</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true"></a><span class="ot">vm ::</span> <span class="dt">Source</span> <span class="op">#</span> <span class="dt">VonMises</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true"></a>vm <span class="ot">=</span> fromTuple (<span class="dv">0</span>,<span class="dv">1</span>)</span></code></pre></div>
<p>so that <code>vm</code> is a von Mises distribution with mean 0 and concentration 1.</p>
<p>In this tutorial we’re not simply mixing von Mises distributions, but rather products of von Mises distributions, so that we can model distributions on a bounded plane. Such a product manifold is defined by the type <code>(VonMises,VonMises)</code>, and we create three such product von Mises distributions with the lines</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true"></a>vm1,vm2,<span class="ot">vm3 ::</span> <span class="dt">Source</span> <span class="op">#</span> (<span class="dt">VonMises</span>,<span class="dt">VonMises</span>)</span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true"></a>vm1 <span class="ot">=</span> fromTuple (<span class="dv">1</span>, <span class="fl">1.5</span>, <span class="fl">4.5</span>, <span class="dv">2</span>)</span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true"></a>vm2 <span class="ot">=</span> fromTuple (<span class="dv">3</span>, <span class="dv">2</span>, <span class="fl">3.5</span>, <span class="dv">3</span>)</span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true"></a>vm3 <span class="ot">=</span> fromTuple (<span class="fl">4.5</span>, <span class="dv">4</span>, <span class="fl">1.5</span>, <span class="dv">2</span>)</span></code></pre></div>
<p>To create a mixture distribution we also need to define weights for the component distributions, which in Goal is represented by the <code>Categorical</code> distribution. We create a Categorical distribution over three categories with the following lines</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true"></a><span class="ot">wghts ::</span> <span class="dt">Source</span> <span class="op">#</span> <span class="dt">Categorical</span> <span class="dv">2</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true"></a>wghts <span class="ot">=</span> fromTuple (<span class="fl">0.33</span>,<span class="fl">0.33</span>)</span></code></pre></div>
<p>where the probability of the first category is given by <code>1 - sum (listCoordinates wghts)</code>.</p>
<p>Finally, we may create a mixture distribution out of these previously defined distributions with the lines</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true"></a><span class="ot">strumxmdl ::</span> <span class="dt">Source</span> <span class="op">#</span> <span class="dt">Mixture</span> (<span class="dt">VonMises</span>,<span class="dt">VonMises</span>) <span class="dv">2</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true"></a>strumxmdl <span class="ot">=</span> joinSourceMixture (S.fromTuple (vm1,vm2,vm3)) wghts</span></code></pre></div>
<p>In Goal, I attempt to define the <code>Source</code> coordinates to match conventional definitions provided in textbooks and on Wikipedia. Typically, when creating example or initial distributions, it is most intuitive to do so in <code>Source</code> coordinates. Neverthless, most algorithms and implementations in Goal are based on exponential family formulations of statistical models, and thus the workhorse coordinate systems of Goal are the <code>Mean</code> and <code>Natural</code> coordinates.</p>
<p>To convert our mixture model from <code>Source</code> coordinates to <code>Natural</code> coordinates, we simply use the <code>transition</code> function</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true"></a><span class="ot">trumxmdl ::</span> <span class="dt">Natural</span> <span class="op">#</span> <span class="dt">Mixture</span> (<span class="dt">VonMises</span>,<span class="dt">VonMises</span>) <span class="dv">2</span></span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true"></a>trumxmdl <span class="ot">=</span> transition strumxmdl</span></code></pre></div>
<p>Having created a mixture distribution, our next task will be to generate some data from this “ground-truth” distribution, and see if we can recover the ground-truth distribution through observation with the power of statistics. To do so we’re going to define a few variables to initialize and train our model.</p>
<p>Firstly, we’re going to want to randomly generate an initial point in our statistical model that we will then optimize. We will choose this point by setting each of its coordinates randomly according to the normal distribution</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true"></a><span class="ot">mxmdlint ::</span> <span class="dt">Source</span> <span class="op">#</span> <span class="dt">Normal</span></span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true"></a>mxmdlint <span class="ot">=</span> fromTuple (<span class="dv">0</span>,<span class="fl">0.1</span>)</span></code></pre></div>
<p>Secondly, there are a handful of variables pertaining to training that we need to define, namely</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true"></a><span class="ot">nsmps ::</span> <span class="dt">Int</span></span>
<span id="cb8-2"><a href="#cb8-2" aria-hidden="true"></a>nsmps <span class="ot">=</span> <span class="dv">100</span></span>
<span id="cb8-3"><a href="#cb8-3" aria-hidden="true"></a></span>
<span id="cb8-4"><a href="#cb8-4" aria-hidden="true"></a><span class="ot">eps ::</span> <span class="dt">Double</span></span>
<span id="cb8-5"><a href="#cb8-5" aria-hidden="true"></a>eps <span class="ot">=</span> <span class="fl">0.05</span></span>
<span id="cb8-6"><a href="#cb8-6" aria-hidden="true"></a></span>
<span id="cb8-7"><a href="#cb8-7" aria-hidden="true"></a><span class="ot">bnd ::</span> <span class="dt">Double</span></span>
<span id="cb8-8"><a href="#cb8-8" aria-hidden="true"></a>bnd <span class="ot">=</span> <span class="fl">1e-5</span></span>
<span id="cb8-9"><a href="#cb8-9" aria-hidden="true"></a></span>
<span id="cb8-10"><a href="#cb8-10" aria-hidden="true"></a><span class="ot">admmlt ::</span> <span class="dt">Int</span></span>
<span id="cb8-11"><a href="#cb8-11" aria-hidden="true"></a>admmlt <span class="ot">=</span> <span class="dv">10</span></span>
<span id="cb8-12"><a href="#cb8-12" aria-hidden="true"></a></span>
<span id="cb8-13"><a href="#cb8-13" aria-hidden="true"></a><span class="co">-- EM</span></span>
<span id="cb8-14"><a href="#cb8-14" aria-hidden="true"></a><span class="ot">nepchs ::</span> <span class="dt">Int</span></span>
<span id="cb8-15"><a href="#cb8-15" aria-hidden="true"></a>nepchs <span class="ot">=</span> <span class="dv">200</span></span></code></pre></div>
<p>but I’ll explain them as we review the training procedures that we’ll be using.</p>
<p>Now, the standard framework for fitting a mixture model is expectation-maximization (EM), and Goal provides the <code>expectationMaximization</code> function which works for a wide class of models. Nevertheless, it requires that the model in question satisfies certain analytic properties, which are not satisfied by von Mises mixtures. For such cases Goal provies the <code>expectationMaximizationAscent</code> function, which approximates the maximization step of EM with gradient descent, and has looser restrictions on the model. We use this function to construct the EM iterator</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true"></a>vonMisesEM</span>
<span id="cb9-2"><a href="#cb9-2" aria-hidden="true"></a><span class="ot">    ::</span> <span class="dt">Sample</span> (<span class="dt">VonMises</span>,<span class="dt">VonMises</span>) <span class="co">-- ^ Observations</span></span>
<span id="cb9-3"><a href="#cb9-3" aria-hidden="true"></a>    <span class="ot">-&gt;</span> <span class="dt">Natural</span> <span class="op">#</span> <span class="dt">Mixture</span> (<span class="dt">VonMises</span>,<span class="dt">VonMises</span>) <span class="dv">2</span></span>
<span id="cb9-4"><a href="#cb9-4" aria-hidden="true"></a>    <span class="ot">-&gt;</span> <span class="dt">Natural</span> <span class="op">#</span> <span class="dt">Mixture</span> (<span class="dt">VonMises</span>,<span class="dt">VonMises</span>) <span class="dv">2</span></span>
<span id="cb9-5"><a href="#cb9-5" aria-hidden="true"></a>vonMisesEM zs nmxmdl <span class="ot">=</span> cauchyLimit euclideanDistance bnd</span>
<span id="cb9-6"><a href="#cb9-6" aria-hidden="true"></a>    <span class="op">$</span> expectationMaximizationAscent eps defaultAdamPursuit zs nmxmdl</span></code></pre></div>
<p>where <code>bnd</code> indicates the the threshold for stopping the gradient ascent, and <code>eps</code> is the learning rate.</p>
<p>An alternative procedure for fitting latent variable models is to simply perform gradient ascent on the marginal log-likelihood of the mixture given data. This procedure fails when applied to Gaussian mixture models, but we will test if it works for von Mises mixtures.</p>
<p>To continue, let us generate <code>nsmps</code> samples from our ground-truth mixture distribution <code>trumxmdl</code>, initialize our model <code>mxmdl0</code>, and collect our observations <code>xys</code>:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true"></a><span class="ot">main ::</span> <span class="dt">IO</span> ()</span>
<span id="cb10-2"><a href="#cb10-2" aria-hidden="true"></a>main <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb10-3"><a href="#cb10-3" aria-hidden="true"></a></span>
<span id="cb10-4"><a href="#cb10-4" aria-hidden="true"></a>    cxys <span class="ot">&lt;-</span> realize <span class="op">$</span> sample nsmps trumxmdl</span>
<span id="cb10-5"><a href="#cb10-5" aria-hidden="true"></a>    mxmdl0 <span class="ot">&lt;-</span> realize <span class="op">$</span> initialize mxmdlint</span>
<span id="cb10-6"><a href="#cb10-6" aria-hidden="true"></a></span>
<span id="cb10-7"><a href="#cb10-7" aria-hidden="true"></a>    <span class="kw">let</span> xys <span class="ot">=</span> <span class="fu">fst</span> <span class="op">&lt;$&gt;</span> cxys</span></code></pre></div>
<p>We we will then run our two procedures for <code>nepchs</code> number of iterations</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true"></a>    <span class="kw">let</span> emmxmdls <span class="ot">=</span> <span class="fu">take</span> nepchs <span class="op">$</span> <span class="fu">iterate</span> (vonMisesEM xys) mxmdl0</span>
<span id="cb11-2"><a href="#cb11-2" aria-hidden="true"></a></span>
<span id="cb11-3"><a href="#cb11-3" aria-hidden="true"></a>    <span class="kw">let</span> admmxmdls <span class="ot">=</span> <span class="fu">take</span> nepchs <span class="op">.</span> takeEvery admmlt</span>
<span id="cb11-4"><a href="#cb11-4" aria-hidden="true"></a>            <span class="op">$</span> vanillaGradientSequence (logLikelihoodDifferential xys) eps defaultAdamPursuit mxmdl0</span></code></pre></div>
<p>In the case of ascending the <code>logLikelihoodDifferential</code>, we multiply the number of steps taken by <code>admmlt</code>, since each iteration of the EM algorithm requires a number of sub steps, and we want a fair comparison of these two algorithms.</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true"></a>    <span class="kw">let</span> emnlls <span class="ot">=</span> logLikelihood xys <span class="op">&lt;$&gt;</span> emmxmdls</span>
<span id="cb12-2"><a href="#cb12-2" aria-hidden="true"></a>        admnlls <span class="ot">=</span> logLikelihood xys <span class="op">&lt;$&gt;</span> admmxmdls</span></code></pre></div>
<figure>
<img src="../../files/posts/goal/von-mises-mixture/log-likelihood-ascent.png" alt="Log-likelihood ascent"> <img src="../../files/posts/goal/von-mises-mixture/confidence-intervals.png" alt="Learned models">
</figure>
<p>We plot the results of our simulations in the above figure. On the left we have the model performance (log-likelihood) as function of number of training iterations, where an “iteration” is one step of EM, or ten steps of gradient pursuit. In contrast with normal distributions, mixtures of von Mises distributions can be trained effectively with gradient pursuit.</p>
<p>On the right side representations of the ground-truth mixture distribution, the 100 sampled data-points from the ground-truth distribution, and the models learned by EM and GP. Indeed, we see that they both learn more-or-less the same results. I’ll note though that the results aren’t always so similar — try running the <a href="https://gitlab.com/sacha-sokoloski/goal/tree/master/scripts/von-mises-mixture">von-mises-mixture</a> script yourself to see.</p>
    </section>
</article>

        </main>

        <footer>
            <p>email: <a href="mailto:sacha.sokoloski@mailbox.org">sacha.sokoloski@mailbox.org</a></p>
            <p>gitlab: <a href="https://gitlab.com/sacha-sokoloski">gitlab.com/sacha-sokoloski</a> </p>
            <p>Site generated by <a href="http://jaspervdj.be/hakyll">Hakyll</a></p>
        </footer>
    </body>
</html>
