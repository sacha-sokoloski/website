<!doctype html>

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js">
</script>


<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sacha Sokoloski - Goal Tutorials</title>
        <link rel="stylesheet" href="../../css/default.css" />
        <link rel="stylesheet" href="../../css/syntax.css" />
        <link rel="stylesheet" href="../../css/float.css" />
    </head>
    <body>
        <header>
            <div class="title">
                <a href="../../">Sacha Sokoloski - Goal Tutorials</a>
            </div>
            <nav>
                <a href="../../">Home</a>
                <a href="../../pages/research.html">Research</a>
                <a href="../../pages/blog.html">Blog</a>
                <a href="../../pages/personal.html">Personal</a>
                <a href="../../pages/cv.html">CV</a>
            </nav>
        </header>

        <main role="main">
            <article>
    <section class="header">
        <h2>Lazy backpropagation</h2>
        
    </section>
    <section>
        <style>

img  {
    width: 49%;
    height: auto;
    object-fit: cover;
}

figure  {
    margin: 1px;
    border: 0.1em grey solid;
}
</style>
<p>In this post we’ll go through the <a href="https://gitlab.com/sacha-sokoloski/goal/tree/master/scripts/neural-network">neural-network</a> script, and see how to fit a simple neural network in Goal. I will then show how I’ve implemented an implicit version of backprop by combining Goal types with lazy evaluation, and thereby avoid explicitly storing and passing gradients. Explaining this implementation of backprop is my primary interest in this tutorial, as it is general, efficient, and (I think) kind of cool.</p>
<!--more-->
<h3 id="a-toy-example">A toy example</h3>
<p>To begin, we’re going to consider the arbitrary function</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true"></a><span class="ot">f ::</span> <span class="dt">Double</span> <span class="ot">-&gt;</span> <span class="dt">Double</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true"></a>f x <span class="ot">=</span> <span class="fu">exp</span> <span class="op">.</span> <span class="fu">sin</span> <span class="op">$</span> <span class="dv">2</span> <span class="op">*</span> x</span></code></pre></div>
<p>and prepare to generate some noisy samples from it</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true"></a><span class="ot">fp ::</span> <span class="dt">Source</span> <span class="op">#</span> <span class="dt">Normal</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true"></a>fp <span class="ot">=</span> fromTuple (<span class="dv">0</span>,<span class="fl">0.1</span>)</span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true"></a></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true"></a>mnx,<span class="ot">mxx ::</span> <span class="dt">Double</span></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true"></a>mnx <span class="ot">=</span> <span class="op">-</span><span class="dv">3</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true"></a>mxx <span class="ot">=</span> <span class="dv">3</span></span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true"></a></span>
<span id="cb2-8"><a href="#cb2-8" aria-hidden="true"></a><span class="ot">xs ::</span> [<span class="dt">Double</span>]</span>
<span id="cb2-9"><a href="#cb2-9" aria-hidden="true"></a>xs <span class="ot">=</span> <span class="fu">concat</span> <span class="op">.</span> <span class="fu">replicate</span> <span class="dv">5</span> <span class="op">$</span> <span class="fu">range</span> mnx mxx <span class="dv">8</span></span></code></pre></div>
<p>The <code>xs</code> here are the inputs in the training data, and we’ll generate the outputs <code>ys</code> once we get to defining <code>main</code>.</p>
<p>The next step is to define and initialize our neural network. We’ll initialize the parameters of our network with the normal distribution</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true"></a><span class="ot">cp ::</span> <span class="dt">Source</span> <span class="op">#</span> <span class="dt">Normal</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true"></a>cp <span class="ot">=</span> fromTuple (<span class="dv">0</span>,<span class="fl">0.1</span>)</span></code></pre></div>
<p>As for the network itself, we may specify its structure with the following type synonym:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true"></a><span class="kw">type</span> <span class="dt">NeuralNetwork'</span> <span class="ot">=</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true"></a>    <span class="dt">NeuralNetwork</span> '[ '(<span class="dt">Tensor</span>, <span class="dt">R</span> <span class="dv">50</span> <span class="dt">Bernoulli</span>)]</span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true"></a>    <span class="dt">Tensor</span> <span class="dt">NormalMean</span> <span class="dt">NormalMean</span></span></code></pre></div>
<p>This defines a model with 1-dimensional inputs and outputs, a linear output layer, and a 50 neuron hidden layer with sigmoid activation functions. The types for neural networks in Goal are somewhat idiosyncratic, but there is a method to the madness. Feel free to skip the next paragraph if you don’t care how to read this type; regardless, please note that the underlying implementation is quite efficient, and interested parties are welcome to contact me if a more standard interface for specifying neural networks is desired.</p>
<p>In Goal, a <code>NeuralNetwork</code> is a type of <code>Map</code>, where <code>Map</code>s are parametric functions. Starting from the right, the <code>NormalMean</code> type indicates that the input of the NeuralNetwork is a 1-dimensional real number. The next <code>NormalMean</code> and the <code>Tensor</code> indicate that the <code>NeuralNetwork</code> has a linear output layer. Finally, the type-list with one element indicates that the network has one layer, and the <code>Tensor</code> type again indicates that it is fully-connected. Finally, <code>R 50 Bernoulli</code> indicates that the layer has 50 neurons, and the transfer function is given by the mapping from <code>Natural</code> to <code>Mean</code> coordinates for the <code>Bernoulli</code> distribution, which happens to be the sigmoid.</p>
<p>Getting back to things, we next have some training parameters</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true"></a><span class="ot">nepchs ::</span> <span class="dt">Int</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true"></a>nepchs <span class="ot">=</span> <span class="dv">1000</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true"></a></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true"></a><span class="ot">eps ::</span> <span class="dt">Double</span></span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true"></a>eps <span class="ot">=</span> <span class="fl">0.05</span></span>
<span id="cb5-6"><a href="#cb5-6" aria-hidden="true"></a></span>
<span id="cb5-7"><a href="#cb5-7" aria-hidden="true"></a><span class="ot">mxmu ::</span> <span class="dt">Double</span></span>
<span id="cb5-8"><a href="#cb5-8" aria-hidden="true"></a>mxmu <span class="ot">=</span> <span class="fl">0.999</span></span></code></pre></div>
<p>which we’ll explain momentarily in context.</p>
<p>Now, the first things we want to do in <code>main</code> are generate some noisy responses, and bundle them up with the inputs to define our training data <code>xys</code></p>
<div class="sourceCode" id="cb6"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true"></a><span class="ot">main ::</span> <span class="dt">IO</span> ()</span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true"></a>main <span class="ot">=</span> <span class="kw">do</span></span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true"></a></span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true"></a>    ys <span class="ot">&lt;-</span> realize <span class="op">$</span> <span class="fu">mapM</span> (noisyFunction fp f) xs</span>
<span id="cb6-5"><a href="#cb6-5" aria-hidden="true"></a></span>
<span id="cb6-6"><a href="#cb6-6" aria-hidden="true"></a>    <span class="kw">let</span> xys <span class="ot">=</span> <span class="fu">zip</span> ys xs</span></code></pre></div>
<p>We then define our cost function as the conditional log-likelihood given these training data</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true"></a>    <span class="kw">let</span><span class="ot"> cost ::</span> <span class="dt">Natural</span> <span class="op">#</span> <span class="dt">NeuralNetwork'</span> <span class="ot">-&gt;</span> <span class="dt">Double</span></span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true"></a>        cost <span class="ot">=</span> conditionalLogLikelihood xys</span></code></pre></div>
<p>and we use its differential</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true"></a>    <span class="kw">let</span><span class="ot"> backprop ::</span> <span class="dt">Natural</span> <span class="op">#</span> <span class="dt">NeuralNetwork'</span> <span class="ot">-&gt;</span> <span class="dt">Natural</span> <span class="op">#*</span> <span class="dt">NeuralNetwork'</span></span>
<span id="cb8-2"><a href="#cb8-2" aria-hidden="true"></a>        backprop <span class="ot">=</span> conditionalLogLikelihoodDifferential xys</span></code></pre></div>
<p>to drive our gradient pursuit algorithms. In the following paragraph I describe some of the details of differentials in goal, but this can also be freely skipped.</p>
<p>So, note that where <code>Natural # NeuralNetwork'</code> is a point in a neural network manifold in natural coordinates, the <code>#*</code> in the differential <code>Natural #* NeuralNetwork'</code> indicates that the differential is in the <code>Dual</code> space of <code>Natural # NeuralNetwork</code>. For exponential families, the dual space of the <code>Natural</code> coordinates are the <code>Mean</code> coordinates. That being said, <code>NeuralNetwork</code>s aren’t really exponential families, and we rather only use exponential family structures as a shortcut for defining them.</p>
<p>Anyway, we then need to initialize our neural network</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true"></a>    mlp0 <span class="ot">&lt;-</span> realize <span class="op">$</span> initialize cp</span></code></pre></div>
<p>and do some gradient pursuit!</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true"></a>    <span class="kw">let</span> sgdmlps <span class="ot">=</span> <span class="fu">take</span> nepchs <span class="op">$</span> vanillaGradientSequence</span>
<span id="cb10-2"><a href="#cb10-2" aria-hidden="true"></a>            backprop eps <span class="dt">Classic</span> mlp0</span>
<span id="cb10-3"><a href="#cb10-3" aria-hidden="true"></a>        mtmmlps <span class="ot">=</span> <span class="fu">take</span> nepchs <span class="op">$</span> vanillaGradientSequence</span>
<span id="cb10-4"><a href="#cb10-4" aria-hidden="true"></a>            backprop eps (defaultMomentumPursuit mxmu) mlp0</span>
<span id="cb10-5"><a href="#cb10-5" aria-hidden="true"></a>        admmlps <span class="ot">=</span> <span class="fu">take</span> nepchs <span class="op">$</span> vanillaGradientSequence</span>
<span id="cb10-6"><a href="#cb10-6" aria-hidden="true"></a>            backprop eps defaultAdamPursuit mlp0</span></code></pre></div>
<figure>
<img src="../../files/posts/goal/neural-network/log-likelihood-ascent.png" alt="Log-likelihood ascent"> <img src="../../files/posts/goal/neural-network/regression.png" alt="Learned models">
</figure>
<p>Just like in my <a href="../../posts/01-gradient-descent.html">introduction to gradient pursuit</a>, we’re going to compare the performance of classic, momentum, and Adam gradient descent algorithms. In this case however, the problem itself is not trivial. And indeed, in contrast with the previous tutorial, here Adam has a clear advantage, and achieves a higher overall log-likelihood, and more exactly captures the true function from which the data was noisily sampled.</p>
<h3 id="lazy-backprop-implementation">Lazy backprop implementation</h3>
<p>Now, having demonstrated that my implementation of neural networks does indeed work, I would like to dig into the implementation. First, let’s review the math of backpropagation. We may think of a neural network as a function <span class="math inline">\(f(x;~\theta)\)</span> where <span class="math inline">\(x\)</span> is an input and <span class="math inline">\(\theta\)</span> are the parameters of the network. Suppose we have a cost function <span class="math inline">\(c(f(x_i\ ; \theta),y_i)\)</span> which tells us how close the neural network value <span class="math inline">\(f(x_i)\)</span> is to the target <span class="math inline">\(y_i\)</span>. Then given some input output data <span class="math inline">\((x_1,y_1), \ldots, (x_n,y_n)\)</span>, we optimize the neural network by maximizing <span class="math inline">\(\sum c(f(x_i\ ; \theta),y_i)\)</span> with respect to <span class="math inline">\(\theta\)</span>, which we do in turn by computing differentials (or derivatives) of <span class="math inline">\(c\)</span> with respect to <span class="math inline">\(\theta\)</span> and using them to iteratively improving the network.</p>
<p>Now, let us focus on a single data point <span class="math inline">\((x,y)\)</span>, and imagine we have a network with <span class="math inline">\(d\)</span> layers, and that the parameters of the network <span class="math inline">\(\theta = (W_1, \ldots, W_d)\)</span> correspond to the weights of each layer of the network. We may then express the differential of <span class="math inline">\(c\)</span> with respect to the weights <span class="math inline">\(W_i\)</span> in layer <span class="math inline">\(i\)</span> as <span class="math display">\[\delta_{W_i} \ c(f(x; \theta),y) = r_i \otimes z_{i-1},\]</span> where <span class="math inline">\(\otimes\)</span> is the outer product, <span class="math inline">\(z_{i-1}\)</span> are the feedforward outputs from the previous layer, and <span class="math inline">\(r_i\)</span> are the backpropagated error differentials from layer <span class="math inline">\(i+1\)</span>.</p>
<p>The tricky part here is two-fold: Firstly, the error differentials <span class="math inline">\(r_i\)</span> depend on the outputs <span class="math inline">\(z_{i-1}\)</span>. Now, we typically would get around such dependencies by passing each <span class="math inline">\(z_{i-1}\)</span> forward and recursively computing <span class="math inline">\(r_i\)</span>. However, our aim is not simply to compute the local errors <span class="math inline">\(r_i\)</span> but rather the derivatives of all the parameters of the network. As such, in a strongly typed-language like Haskell, our second problem is that any recursive function for computing the differentials of a whole network’s parameters has to know the type of the whole network in advance. This subtlety means that most implementations of backprop in Haskell (as far as I know), rely on explicitly implementing and storing the results of the forward and backward passes, and then collecting the results into the network differential. Theoretically though, this shouldn’t be necessary, because the differential for the weights <span class="math inline">\(W_i\)</span> is fundamentally local and recursive.</p>
<p>We can implement this without explicitly storing values of <span class="math inline">\(z\)</span> by leveraging Goal types and laziness. In Goal, a <code>Manifold</code> is a <code>Map</code> if it can be applied to some inputs on a <code>Manifold</code> to return some outputs on another <code>Manifold</code> (the functions for doing that are <code>&gt;.&gt;</code> and the batch version <code>&gt;$&gt;</code>). A <code>Map</code> is then an instance of <code>Propagate</code> if it has a <code>propagate</code> function</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true"></a><span class="kw">class</span> <span class="dt">Map</span> c f y x <span class="ot">=&gt;</span> <span class="dt">Propagate</span> c f y x <span class="kw">where</span></span>
<span id="cb11-2"><a href="#cb11-2" aria-hidden="true"></a><span class="ot">    propagate ::</span> [c <span class="op">#*</span> y] <span class="co">-- ^ The error differential</span></span>
<span id="cb11-3"><a href="#cb11-3" aria-hidden="true"></a>              <span class="ot">-&gt;</span> [c <span class="op">#*</span> x] <span class="co">-- ^ A vector of inputs</span></span>
<span id="cb11-4"><a href="#cb11-4" aria-hidden="true"></a>              <span class="ot">-&gt;</span> c <span class="op">#</span> f y x <span class="co">-- ^ The function (e.g. NN) to differentiate</span></span>
<span id="cb11-5"><a href="#cb11-5" aria-hidden="true"></a>              <span class="ot">-&gt;</span> (c <span class="op">#*</span> f y x, [c <span class="op">#</span> y]) <span class="co">-- ^ The derivative, and function output</span></span></code></pre></div>
<p>which takes differentials <code>[c #* y]</code> on the output manifold <code>y</code> (these are the <span class="math inline">\(r_i\)</span>), the inputs that trigged those errors <code>[c #* x]</code> (these are the <span class="math inline">\(z_i\)</span>), and the <code>Map</code> to differentiate <code>c # f y x</code> (this is the whole network). Finally, it returns the differential <code>c #* f y x</code> of the <code>Map</code>, as well as the outputs <code>[c # y]</code> of the <code>Map</code>.</p>
<p>Now, given an instance of <code>Propagate</code>, we may use the <code>backpropagation</code> function</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true"></a>backpropagation</span>
<span id="cb12-2"><a href="#cb12-2" aria-hidden="true"></a><span class="ot">    ::</span> <span class="dt">Propagate</span> c f y x</span>
<span id="cb12-3"><a href="#cb12-3" aria-hidden="true"></a>    <span class="ot">=&gt;</span> (a <span class="ot">-&gt;</span> c <span class="op">#</span> y <span class="ot">-&gt;</span> c <span class="op">#*</span> y)</span>
<span id="cb12-4"><a href="#cb12-4" aria-hidden="true"></a>    <span class="ot">-&gt;</span> [(a, c <span class="op">#*</span> x)]</span>
<span id="cb12-5"><a href="#cb12-5" aria-hidden="true"></a>    <span class="ot">-&gt;</span> c <span class="op">#</span> f y x</span>
<span id="cb12-6"><a href="#cb12-6" aria-hidden="true"></a>    <span class="ot">-&gt;</span> c <span class="op">#*</span> f y x</span>
<span id="cb12-7"><a href="#cb12-7" aria-hidden="true"></a><span class="ot">{-# INLINE backpropagation #-}</span></span>
<span id="cb12-8"><a href="#cb12-8" aria-hidden="true"></a>backpropagation grd ysxs f <span class="ot">=</span></span>
<span id="cb12-9"><a href="#cb12-9" aria-hidden="true"></a>    <span class="kw">let</span> (yss,xs) <span class="ot">=</span> <span class="fu">unzip</span> ysxs</span>
<span id="cb12-10"><a href="#cb12-10" aria-hidden="true"></a>        (df,yhts) <span class="ot">=</span> propagate dys xs f</span>
<span id="cb12-11"><a href="#cb12-11" aria-hidden="true"></a>        dys <span class="ot">=</span> <span class="fu">zipWith</span> grd yss yhts</span>
<span id="cb12-12"><a href="#cb12-12" aria-hidden="true"></a>     <span class="kw">in</span> df</span></code></pre></div>
<p>which takes a function for computing differentials (the differential of <span class="math inline">\(c\)</span> at <span class="math inline">\(f(x)\)</span>), some output/input pairs and the <code>Map</code> <code>c # f y x</code>, and then it returns the differential <code>c #* f y x</code>. Notice the laziness the definition of the function: the <code>propagate</code> function uses the error differentials <code>dys</code> and returns the outputs <code>yhts</code>, while at the same time, the error differentials depend on the outputs <code>yhts</code>.</p>
<p>Finally, let’s look at how a <code>NeuralNetwork</code> implements <code>propagate</code> (apologies, but note that the variable notation here is slightly different than what we saw earlier)</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true"></a><span class="kw">instance</span></span>
<span id="cb13-2"><a href="#cb13-2" aria-hidden="true"></a>    ( <span class="dt">Propagate</span> c f z y, <span class="dt">Propagate</span> c (<span class="dt">NeuralNetwork</span> gys g) y x, <span class="dt">Map</span> c f y z</span>
<span id="cb13-3"><a href="#cb13-3" aria-hidden="true"></a>    , <span class="dt">Transition</span> c (<span class="dt">Dual</span> c) y, <span class="dt">Legendre</span> y, <span class="dt">Riemannian</span> c y, <span class="dt">Bilinear</span> f z y)</span>
<span id="cb13-4"><a href="#cb13-4" aria-hidden="true"></a>  <span class="ot">=&gt;</span> <span class="dt">Propagate</span> c (<span class="dt">NeuralNetwork</span> ('(g,y) <span class="op">:</span> gys) f) z x <span class="kw">where</span></span>
<span id="cb13-5"><a href="#cb13-5" aria-hidden="true"></a>      <span class="ot">{-# INLINE propagate #-}</span></span>
<span id="cb13-6"><a href="#cb13-6" aria-hidden="true"></a>      propagate dzs xs fg <span class="ot">=</span></span>
<span id="cb13-7"><a href="#cb13-7" aria-hidden="true"></a>          <span class="kw">let</span> (f,g) <span class="ot">=</span> split fg</span>
<span id="cb13-8"><a href="#cb13-8" aria-hidden="true"></a>              fmtx <span class="ot">=</span> <span class="fu">snd</span> <span class="op">$</span> split f</span>
<span id="cb13-9"><a href="#cb13-9" aria-hidden="true"></a>              mys <span class="ot">=</span> transition <span class="op">&lt;$&gt;</span> ys</span>
<span id="cb13-10"><a href="#cb13-10" aria-hidden="true"></a>              (df,zhts) <span class="ot">=</span> propagate dzs mys f</span>
<span id="cb13-11"><a href="#cb13-11" aria-hidden="true"></a>              (dg,ys) <span class="ot">=</span> propagate dys xs g</span>
<span id="cb13-12"><a href="#cb13-12" aria-hidden="true"></a>              dys0 <span class="ot">=</span> dzs <span class="op">&lt;$&lt;</span> fmtx</span>
<span id="cb13-13"><a href="#cb13-13" aria-hidden="true"></a>              dys <span class="ot">=</span> <span class="fu">zipWith</span> flat ys dys0</span>
<span id="cb13-14"><a href="#cb13-14" aria-hidden="true"></a>           <span class="kw">in</span> (join df dg, zhts)</span></code></pre></div>
<p>Again, there are some details here that I will elide, but I hope the core story still shines through. So to begin, line by line, <code>dzs</code> are the error differentials on the output, <code>xs</code> are the inputs, and <code>fg</code> is the neural network.</p>
<p>The first thing we do is <code>split</code> <code>fg</code> into the top layer <code>f</code>, and the rest of the network <code>g</code>. <code>f</code> is a linear function and a shift, and we’ll need to <code>split</code> off the linear part <code>fmtx</code>. The next line produces <code>mys</code> (which are inputs to <code>f</code>) by applying the transfer, or, <code>transition</code> function (e.g. a sigmoid) to the linear output <code>ys</code> of <code>g</code>. We next apply <code>propagate</code> to the error differentials <code>dzs</code>, the <code>mys</code> we just computed, and the top layer <code>f</code>, to compute the the differential <code>df</code>, and the estimated outputs <code>zhts</code>. We also apply <code>propagate</code> to the error differentials <code>dys</code> — which are differentials at the output of <code>g</code>/input of <code>f</code> — the inputs <code>xs</code>, and the neural network <code>g</code>. Finally, to compute the error differentials <code>dys</code>, we apply the transpose of <code>fmtx</code> to the error differentials <code>dzs</code>, and combine the results with <code>ys</code> by using the <code>flat</code> function. <code>flat</code> is a bit of verbiage from Riemannian geometry, but sufficed to say, when the transfer function is a sigmoid it computes the derivative of the sigmoid function at the given <code>ys</code>. Finally, we may gather our differentials by <code>join</code>ing <code>df</code> and <code>dg</code>, and returning the outputs <code>zhts</code>.</p>
<p>So, how does this work? After the splitting of the neural network <code>fg</code>, we may rewrite this instance as two calls of propagate: <code>propagate dzs (transition &lt;$&gt; ys) f</code> and <code>propagate (zipWith flat ys $ dzs &lt;$&lt; snd (split f)) xs g</code>. These calls depend on each other: we need the output of <code>g</code> to feed-forward through <code>f</code>, and we need to backpropagate <code>dzs</code> through <code>f</code> to compute the differentials <code>dys</code>. This mutual dependence allows us to solve the two-fold problem with backprop we discussed earlier: the first backpropagate solves the dependence of <span class="math inline">\(r_i\)</span> on <span class="math inline">\(z_i\)</span>, and the second dependence solves the problem of maintaining the whole network structure, by returning the differential of the lower layers <code>g</code>, and allowing the <code>propagate</code> instance to combine them with the differential of <code>f</code>. As far as I understand (and I’m not at all an expert here), this is a lazy implementation of reverse-mode automatic differentiation.</p>
    </section>
</article>

        </main>

        <footer>
            <p>email: <a href="mailto:sacha.sokoloski@mailbox.org">sacha.sokoloski@mailbox.org</a></p>
            <p>gitlab: <a href="https://gitlab.com/sacha-sokoloski">gitlab.com/sacha-sokoloski</a> </p>
            <p>Site generated by <a href="http://jaspervdj.be/hakyll">Hakyll</a></p>
        </footer>
    </body>
</html>
