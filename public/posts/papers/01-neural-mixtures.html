<!doctype html>

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js">
</script>


<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sacha Sokoloski - Paper Overviews</title>
        <link rel="stylesheet" href="../../css/default.css" />
        <link rel="stylesheet" href="../../css/syntax.css" />
        <link rel="stylesheet" href="../../css/float.css" />
    </head>
    <body>
        <header>
            <div class="title">
                <a href="../../">Sacha Sokoloski - Paper Overviews</a>
            </div>
            <nav>
                <a href="../../">Home</a>
                <a href="../../pages/research.html">Research</a>
                <a href="../../pages/blog.html">Blog</a>
                <a href="../../pages/personal.html">Personal</a>
                <a href="../../pages/cv.html">CV</a>
            </nav>
        </header>

        <main role="main">
            <article>
    <section class="header">
        <h2>Modelling the neural code in large populations of correlated neurons</h2>
        
    </section>
    <section>
        <style>

img  {
    width: 49%;
    height: auto;
    object-fit: cover;
}

figure  {
    margin: 1px;
    border: 0.1em grey solid;
}

img.box  {
    width: 98%;
    height: auto;
}

figure.box {
    float: right;
    width:49%;
    margin-left: 20px;
}

</style>
<p>In this post I’m going to show how to use the <a href="https://gitlab.com/sacha-sokoloski/neural-mixtures"><code>neural-mixtures</code></a> command-line interface (CLI) to generate the key results and corresponding figures from my paper <em>Modelling the neural code in large populations of correlated neurons</em> <span class="citation" data-cites="sokoloski_modelling_2021">(Sokoloski, Aschner, and Coen-Cagli <a href="#ref-sokoloski_modelling_2021" role="doc-biblioref">2021</a>)</span>. Along the way I will also explain how interested users can format new datasets for use with the <code>neural-mixtures</code> CLI.</p>
<!--more-->
<h3 id="introduction">Introduction</h3>
<p>The goal of my work is provide a model of neural spike-count responses to stimuli that</p>
<ul>
<li>is analytically tractable,</li>
<li>captures first, second, and even higher-order neural response statistics,</li>
<li>captures response features used by the neural code,</li>
<li>and efficiently scales to large populations of neurons.</li>
</ul>
<p>In the following paragraphs I will demonstrate how I have achieved these aims, and how new users can apply my models to new data. To ease navigation, I’ve broken this post down into the following sections:</p>
<ol start="0" type="1">
<li><p><a href="#formatting-new-datasets-for-use-with-the-neural-mixtures-cli">Formatting datasets for use with the neural-mixtures CLI</a></p></li>
<li><p><a href="#installation">Installation</a></p></li>
</ol>
<ol type="i">
<li><a href="#installation-on-linuxmacos">Installation on Linux/macOS</a></li>
<li><a href="#installation-on-windows">Installation on Windows</a></li>
</ol>
<ol start="2" type="1">
<li><a href="#cli-introduction-and-creating-a-toy-experiment">CLI introduction and creating a toy experiment</a></li>
</ol>
<ol type="i">
<li><a href="#synthesizing-an-experiment">Synthesizing an experiment</a></li>
<li><a href="#analyzing-synthetic-data">Analyzing synthetic data</a></li>
</ol>
<ol start="3" type="1">
<li><a href="#modelling-stimulus-response-recordings-with-conditional-mixtures">Modelling stimulus-response recordings with conditional mixtures</a></li>
</ol>
<ol type="i">
<li><a href="#training-conditional-mixtures-on-v1-response-data">Training conditional mixtures on V1 response data</a></li>
<li><a href="#cross-validating-conditional-mixtures">Cross-validating conditional mixtures</a></li>
</ol>
<ol start="4" type="1">
<li><a href="#additional-notes-on-working-with-the-neural-mixtures-library">Additional notes on working with the neural-mixtures library</a></li>
</ol>
<ol type="i">
<li><a href="#multi-threading-and-performance-optimization">Multi-threading and performance optimization</a></li>
<li><a href="#performance-scaling-and-computational-complexity">Performance scaling and computational complexity</a></li>
<li><a href="#modifying-the-plots-and-code">Modifying the plots and code</a></li>
</ol>
<h4 id="formatting-datasets-for-use-with-the-neural-mixtures-cli">Formatting datasets for use with the neural-mixtures CLI</h4>
<p>Before we begin the tutorial proper, I’ll explain how to format datasets for use with the <code>neural-mixtures</code> CLI. After installing <code>neural-mixtures</code>, and as we try out the CLI on the included datasets, you may substitute your own datasets into the commands to begin analyzing your data with <code>neural-mixtures</code>.</p>
<p>Data in <code>neural-mixtures</code> is always organized in a simple directory structure relative to the root of the <code>neural-mixtures</code> repository. Suppose we have a {dataset} from an {experiment}, where the {dataset} is a collection of trials, and where each trial corresponds to a single stimulus-response recording. To analyze the {dataset} with <code>neural-mixtures</code>, create the following plain text files relative to the root of the <code>neural-mixtures</code> repository:</p>
<ul>
<li><code>experiments/{experiment}/{dataset}/stimuli.csv</code> where:
<ul>
<li>The index of the line of the file corresponds to the index of the trial.</li>
<li>Each line is a single stimulus value. Note that the stimulus-value is treated by <code>neural-mixtures</code> as a text string, and can be arbitrary.</li>
</ul></li>
<li><code>experiments/{experiment}/{dataset}/responses.csv</code> where:
<ul>
<li>The index of the line of the file corresponds to the index of the trial.</li>
<li>Each line is a comma (,) seperated list of natural numbers, where each number is the response of a single neuron.</li>
</ul></li>
</ul>
<p>Any programming language should have tools for outputting matrices of data as comma-separated text files. In this case, the data in <code>stimuli.csv</code> corresponds a matrix with one column, and the data in <code>responses.csv</code> corresponds to a matrix with <span class="math inline">\(d_N\)</span> columns, where <span class="math inline">\(d_N\)</span> is the number of recorded neurons.</p>
<p>The <code>train</code>, <code>analyze</code>, and <code>cross-validate</code> programs can then be applied to these files by running e.g.</p>
<p><code>stack exec -- train {experiment} {dataset}</code></p>
<p>and setting the program arguments as desired.</p>
<h3 id="installation">Installation</h3>
<p>I developed <code>neural-mixtures</code> in the Haskell programming language, but installing and applying the <code>neural-mixtures</code> CLI to data does not require any knowledge of Haskell. Managing Haskell packages is automated by the “Haskell Tool Stack” (<code>stack</code> for short). <code>stack</code> can be installed on Linux and macOS by running</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true"></a><span class="ex">curl</span> -sSL https://get.haskellstack.org/ <span class="kw">|</span> <span class="fu">sh</span></span></code></pre></div>
<p>from a terminal. Windows users may run the installer provided <a href="https://get.haskellstack.org/stable/windows-x86_64-installer.exe">here</a>.</p>
<h4 id="installation-on-linuxmacos">Installation on Linux/macOS</h4>
<p><code>stack</code> reduces compiling most Haskell code to a single single command. However, <code>neural-mixtures</code> also depends on non-Haskell code for numerical computation and plotting. These tools ensure that <code>neural-mixtures</code> is fast, but they cannot be installed automatically by <code>stack</code>. What we need are</p>
<ul>
<li>some form of BLAS and LAPACK for linear algebra,</li>
<li>the GNU Scientific Libraries (GSL) for numerics,</li>
<li>and <code>gnuplot</code> for plotting.</li>
</ul>
<p>These are all very standard, and if your target computer is used to perform scientific computing, they are likely already installed.</p>
<p>I use Arch Linux, which provides the <code>pacman</code> package manager, and I use the OpenBLAS implementation of BLAS. As such, on my system I install the relevant packages with</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true"></a><span class="ex">pacman</span> -S gsl lapack openblas gnuplot</span></code></pre></div>
<p>For Debian-based systems this should be something like</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true"></a><span class="ex">apt-get</span> install libgsl-dev liblapack-dev libopenblas-dev gnuplot</span></code></pre></div>
<p>and for macOS,</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true"></a><span class="ex">brew</span> install gsl lapack openblas gnuplot</span></code></pre></div>
<p>should do the trick.</p>
<p>Now, once you have installed <code>stack</code> and the aforementioned numerics libraries, <code>neural-mixtures</code> may be installed by by running</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true"></a><span class="fu">git</span> clone https://gitlab.com/sacha-sokoloski/neural-mixtures</span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true"></a><span class="bu">cd</span> neural-mixtures</span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true"></a><span class="ex">stack</span> build</span></code></pre></div>
<p>Installation will take some time, but should work if the numerics libraries were properly installed. Please let me know if any errors arise on your machine.</p>
<h4 id="installation-on-windows">Installation on Windows</h4>
<p>For Windows things are more complicated, and require using the MSYS2 tools, which provide a Unix-style environment for Windows. Helpfully, <code>stack</code> comes with an installation of MSYS2 that simplifies things. For reference, the following is based on my experience setting up <code>neural-mixtures</code> on an Thinkpad X1 Carbon (8th Gen) running Windows 10.</p>
<p>After running the <a href="https://get.haskellstack.org/stable/windows-x86_64-installer.exe"><code>stack</code> installer</a>, the first step is to locate its MSYS2 install. The directory where <code>stack</code> stores its programs can be located by executing</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode powershell"><code class="sourceCode powershell"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true"></a>stack path --programs</span></code></pre></div>
<p>from the Windows command prompt. This will trigger <code>stack</code> to install MSYS2, after which it will display the directory, which should be something like:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode powershell"><code class="sourceCode powershell"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true"></a>C:\Users\{username}\AppData\Local\Programs\stack\x86_64-windows</span></code></pre></div>
<p>Within this directory there should be an install of MSYS2, with a name like <code>msys2-20210604</code>, where the number corresponds to the date of the particular MSYS2 build provided by the current version of <code>stack</code>. As such, you want to open a directory with the form</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode powershell"><code class="sourceCode powershell"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true"></a>C:\Users\{username}\AppData\Local\Programs\stack\x86_64-windows\msys2-{builddate}</span></code></pre></div>
<p>Within this directory is the <code>msys2_shell</code> command, which is the program we will use to run the <code>neural-mixtures</code> CLI. If you plan on making frequent use of <code>neural-mixtures</code>, I recommend adding a link to <code>msys2_shell</code> to your desktop or some other convenient place. Note that from this point on we no longer require the Windows command prompt.</p>
<p>Now that we have installed <code>stack</code> and a Unix-like environment, we need to install the aforementioned libraries, as well as some development tools. MSYS2 actually uses the <code>pacman</code> package manager that was developed for Arch Linux, and from here on in the installation process will start to look like that of Linux. Run</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true"></a><span class="ex">pacman</span> -S base-devel p7zip git mingw-w64-x86_64-openblas mingw-w64-x86_64-gsl mingw-w64-x86_64-gnuplot</span></code></pre></div>
<p>to install basic development tools and the <code>neural-mixtures</code> dependencies.</p>
<p>Next, to run <code>stack</code> from within the MSYS2 environment, we run the installer for Unix-like systems</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true"></a><span class="ex">curl</span> -sSL https://get.haskellstack.org/ <span class="kw">|</span> <span class="fu">sh</span></span></code></pre></div>
<p>Although it’s also possible to run <code>stack</code> from the Windows command prompt, and use it to install <code>neural-mixtures</code>, I designed the <code>neural-mixtures</code> CLI to be run in a Unix-like environment, and I’d recommend sticking with MSYS2 to avoid errors.</p>
<p>Anyway, we’re now ready to build and install <code>neural-mixtures</code>. In the MSYS2 prompt, run the commands</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true"></a><span class="fu">git</span> clone https://gitlab.com/sacha-sokoloski/neural-mixtures</span>
<span id="cb11-2"><a href="#cb11-2" aria-hidden="true"></a><span class="bu">cd</span> neural-mixtures</span>
<span id="cb11-3"><a href="#cb11-3" aria-hidden="true"></a><span class="ex">stack</span> build --flag hmatrix:openblas --flag hmatrix-gsl:onlygsl</span></code></pre></div>
<p>These extra flags help <code>stack build</code> find the requisite libraries in our Frankenstein development environment. Having gone through these steps, the rest of this tutorial is platform-independent. One last note: if you’re looking for the <code>neural-mixtures</code> repository from the standard Windows environment (so that you can find e.g. the plots generated by the CLI), you will find it under</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode powershell"><code class="sourceCode powershell"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true"></a>msys2-{builddate}/home/{username}/neural-mixtures</span></code></pre></div>
<p>relative to <code>stack</code>’s MSYS2 install.</p>
<h3 id="cli-introduction-and-creating-a-toy-experiment">CLI introduction and creating a toy experiment</h3>
<p>Installing <code>neural-mixtures</code> compiles four command-line programs: <code>synthesize</code>, <code>train</code>, <code>analyze</code>, and <code>cross-validate</code>. These can be run in the root of the <code>neural-mixtures</code> repository by using the <code>stack exec --</code> command, followed by the program name and its arguments (the double dash <code>--</code> separates the arguments for the given program from the arguments for <code>stack</code> itself). One argument accepted by all programs is <code>--help</code>, which will output a long (and hopefully helpful) description of the various arguments that the given program can accept.</p>
<h4 id="synthesizing-an-experiment">Synthesizing an experiment</h4>
<p>To introduce the CLI, we will focus on the <code>synthesize</code> program, which allows us to create a ground-truth models and toy datasets for use with the rest of the CLI. We generate the help output for the <code>synthesize</code> command by running</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true"></a><span class="ex">stack</span> exec -- synthesize --help</span></code></pre></div>
<p>I won’t reproduce the entire output of the help command here, but here is the list of arguments that <code>synthesize</code> accepts:</p>
<div class="sourceCode" id="cb14"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb14-1"><a href="#cb14-1" aria-hidden="true"></a><span class="ex">Usage</span>: synthesize [-e<span class="kw">|</span><span class="ex">--experiment-name</span> ARG] [-n<span class="kw">|</span><span class="ex">--n-neurons</span> ARG]</span>
<span id="cb14-2"><a href="#cb14-2" aria-hidden="true"></a>                  [<span class="ex">-k</span><span class="kw">|</span><span class="ex">--k-components</span> ARG] [-s<span class="kw">|</span><span class="ex">--n-oris</span> ARG] [-S<span class="kw">|</span><span class="ex">--n-samples</span> ARG]</span>
<span id="cb14-3"><a href="#cb14-3" aria-hidden="true"></a>                  [<span class="ex">-N</span><span class="kw">|</span><span class="ex">--sensory-noise</span> ARG] [-g<span class="kw">|</span><span class="ex">--gain-mu</span> ARG] [-G<span class="kw">|</span><span class="ex">--gain-vr</span> ARG]</span>
<span id="cb14-4"><a href="#cb14-4" aria-hidden="true"></a>                  [<span class="ex">-p</span><span class="kw">|</span><span class="ex">--precision-mu</span> ARG] [-P<span class="kw">|</span><span class="ex">--log-precision-vr</span> ARG]</span>
<span id="cb14-5"><a href="#cb14-5" aria-hidden="true"></a>                  [<span class="ex">-h</span><span class="kw">|</span><span class="ex">--min-shape</span> ARG] [-H<span class="kw">|</span><span class="ex">--max-shape</span> ARG]</span></code></pre></div>
<p>In summary, we can specify a name for this synthetic experiment, properties of the generated model such as average tuning properties and number of mixture components, and properties of the synthesized data such as number of stimulus conditions and sample size.</p>
<p>In the <code>neural-mixtures</code> CLI, an experiment is a directory that lives in the <code>experiments</code> directory, at the root of the <code>neural-mixtures</code> repository. An experiment directory contains a set of dataset directories, and each dataset directory contains a <code>stimuli.csv</code> file and a <code>responses.csv</code> file that list presented stimuli and recorded neural responses, respectively. Data and plots generated by the <code>neural-mixtures</code> CLI are also stored in the dataset directories.</p>
<p>Let’s go ahead and execute the command</p>
<div class="sourceCode" id="cb15"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb15-1"><a href="#cb15-1" aria-hidden="true"></a><span class="ex">stack</span> exec -- synthesize</span></code></pre></div>
<p>which runs <code>synthesize</code> with all of its default arguments. The default experiment name for <code>synthesize</code> is <code>synthetic</code>, and so if this command executed successfully, there should be a new directory at <code>experiments/synthetic</code>. Within the <code>synthetic</code> directory there should be the following files</p>
<div class="sourceCode" id="cb16"><pre class="sourceCode powershell"><code class="sourceCode powershell"><span id="cb16-1"><a href="#cb16-1" aria-hidden="true"></a>experiments/synthetic/poisson/stimuli.<span class="fu">csv</span></span>
<span id="cb16-2"><a href="#cb16-2" aria-hidden="true"></a>experiments/synthetic/poisson/responses.<span class="fu">csv</span></span>
<span id="cb16-3"><a href="#cb16-3" aria-hidden="true"></a>experiments/synthetic/poisson/true/parameters.<span class="fu">dat</span></span>
<span id="cb16-4"><a href="#cb16-4" aria-hidden="true"></a>experiments/synthetic/com-based/stimuli.<span class="fu">csv</span></span>
<span id="cb16-5"><a href="#cb16-5" aria-hidden="true"></a>experiments/synthetic/com-based/responses.<span class="fu">csv</span></span>
<span id="cb16-6"><a href="#cb16-6" aria-hidden="true"></a>experiments/synthetic/com-based/true/parameters.<span class="fu">dat</span></span></code></pre></div>
<p>The <code>synthesize</code> program always synthesizes two datasets, which are stored in the <code>poisson</code> and <code>com-based</code> directories. These datasets are generated from conditional mixtures of Poisson distributions and Conway-Maxwell Poisson distributions, respectively (see <span class="citation" data-cites="sokoloski_modelling_2021">Sokoloski, Aschner, and Coen-Cagli (<a href="#ref-sokoloski_modelling_2021" role="doc-biblioref">2021</a>)</span>). The <code>synthesize</code> program also saves the ground-truth parameters of each randomized conditional mixture in the files <code>poisson/true/parameters.dat</code> and <code>com-based/true/parameters.dat</code>.</p>
<h4 id="analyzing-synthetic-data">Analyzing synthetic data</h4>
<p>The <code>analyze</code> program generates collections of plots given a dataset and a model, and we may use it to compare the ground-truth models to their synthesized data in our <code>synthetic</code> experiment. The first arguments to the <code>analyze</code> program are the list of directories in which an appropriate <code>parameters.dat</code> file is located. For example, we may analyze the models we generated with the <code>synthesize</code> command by executing</p>
<div class="sourceCode" id="cb17"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb17-1"><a href="#cb17-1" aria-hidden="true"></a><span class="ex">stack</span> exec -- analyze synthetic poisson true</span>
<span id="cb17-2"><a href="#cb17-2" aria-hidden="true"></a><span class="ex">stack</span> exec -- analyze synthetic com-based true</span></code></pre></div>
<p><code>analyze</code> generates the same set of plots for both commands, so for the sake of brevity we’ll review some of the results of only the <code>com-based</code> analysis. All of these results can be found in the <code>com-based/true</code> directory.</p>
<p>In the paper, a conditional mixture (CM) is a model of the form <span class="math inline">\(p(\mathbf n, k \mid x)\)</span>, where <span class="math inline">\(\mathbf n\)</span> is a vector of spike-counts, <span class="math inline">\(k\)</span> is the index of a mixture-component, and <span class="math inline">\(x\)</span> is the stimulus; we refer to the CoM-based conditional mixture, in particular, as a CB-CM. The <code>analyze</code> command renders the “tuning curves” of the given CM (i.e. the stimulus-dependent firing rate of each neuron) in the file <code>tuning-curves.png</code>, and renders the stimulus-dependent probabilities of the mixture components <span class="math inline">\(p(k \mid x)\)</span> in the file <code>weight-dependence.png</code>.</p>
<figure>
<img src="../../files/posts/papers/neural-mixtures/tuning-curves.png" alt="tuning-curves.png"> <img src="../../files/posts/papers/neural-mixtures/weight-dependence.png" alt="weight-dependence.png">
<figcaption class="float">
Left: Tuning curves of each of the <span class="math inline">\(d_N = 20\)</span> neurons of our synthetic mixture. Right: stimulus-dependent weight of each of the <span class="math inline">\(d_K = 5\)</span> mixture components.
</figcaption>
</figure>
<figure class="box" style="margin-top: 20px">
<img class="box" src="../../files/posts/papers/neural-mixtures/fisher-information.png" alt="fisher-information.png">
<figcaption class="float">
True model Fisher information, as well as linear Fisher information and the bias-corrected empirical estimate.
</figcaption>
</figure>
<p>The “Fisher information” (FI) is a mathematical tool for understanding how much information a given neural population has about a stimulus. The linear Fisher information (LFI) is a linear approximation of the FI which can be estimated effectively from data, and is useful when the FI of a model cannot be evaluated in closed-form. Nevertheless, for CMs the FI can be computed in closed-form. In the <code>fisher-information.png</code> file, the <code>analyze</code> command visualizes both the FI and LFI of the model, as well as a bias-corrected empirical estimate of the LFI <span class="citation" data-cites="kanitscheider_measuring_2015">(Kanitscheider et al. <a href="#ref-kanitscheider_measuring_2015" role="doc-biblioref">2015</a>)</span> based on the synthetic data generated by the <code>synthesize</code> command.</p>
<p>The <code>analyze</code> command also generates the <code>fano-factor-scatter.gif</code> and <code>noise-correlation-scatter.gif</code> files. These files are animations, and render scatter plots of the Fano factors and noise correlations of the model, as compared to the empirical Fano factors and noise correlations, where each step of the animation corresponds to a different stimulus.</p>
<figure>
<img src="../../files/posts/papers/neural-mixtures/fano-factor-scatter.gif" alt="fano-factor-scatter.gif"> <img src="../../files/posts/papers/neural-mixtures/noise-correlation-scatter.gif" alt="noise-correlation-scatter.gif">
<figcaption class="float">
Left: Scatter plots and <span class="math inline">\(r^2\)</span> of model vs. data Fano factors, animated over stimuli. Right: Scatter plots and <span class="math inline">\(r^2\)</span> of model vs. data noise correlations.
</figcaption>
</figure>
<p>It is interesting to note that the <span class="math inline">\(r^2\)</span> of these scatter plots is quite modest, indicating that even with a sample size of 1600, the empirical estimates of the Fano factors and noise correlations are far from that of the ground-truth model.</p>
<h3 id="modelling-stimulus-response-recordings-with-conditional-mixtures">Modelling stimulus-response recordings with conditional mixtures</h3>
<p>In the rest of the tutorial we’ll analyze response recordings from macaque primary visual cortex (V1), but feel free to substitute your own datasets into the <code>neural-mixtures</code> CLI to see what happens!</p>
<h4 id="training-conditional-mixtures-on-v1-response-data">Training conditional mixtures on V1 response data</h4>
<p>Now that we know how to analyze data a CM with <code>neural-mixtures</code>, let’s fit one to data. In <span class="citation" data-cites="sokoloski_modelling_2021">(Sokoloski, Aschner, and Coen-Cagli <a href="#ref-sokoloski_modelling_2021" role="doc-biblioref">2021</a>)</span> we considered two recordings from macaque V1 responding to oriented gratings — one where the monkey is under anaesthesia, and one where it is awake. The <code>stimuli.csv</code> and <code>responses.csv</code> files for these two experiments are available under <code>experiments/amir-anaesthetized/129r001p173_preprocess</code> and <code>experiments/amir-awake-filtered/cadetv1p438_tuning</code>, respectively.</p>
<p>Let’s fit a CB-CM to the <code>cadetv1p438_tuning</code> data, by running the command</p>
<div class="sourceCode" id="cb18"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb18-1"><a href="#cb18-1" aria-hidden="true"></a><span class="ex">stack</span> exec -- train amir-awake-filtered cadetv1p438_tuning discrete com-based -k 20 -e 1000</span></code></pre></div>
<p>To break this down, the first two arguments <code>amir-awake-filtered</code> <code>cadetv1p438_tuning</code> indicate the dataset we will be analyzing. The next argument <code>discrete</code> indicates that we’ll be using discrete tuning curves (which make no assumption about the continuity of the stimulus like von Mises tuning curves), and <code>com-based</code> indicates we’ll be using <code>com-based</code>, rather than <code>poisson</code> tuning curves. The arguments <code>-k 20</code> and <code>-e 1000</code> tell <code>train</code> to train a model with 20 mixture components for 1000 epochs. All the parameters to <code>train</code> are described with the <code>--help</code> command.</p>
<figure class="box">
<img class="box" src="../../files/posts/papers/neural-mixtures/log-likelihood-ascent.png" alt="Log-likelihood ascent.png">
<figcaption class="float">
The log-likelihood of the CB-CM over training epochs.
</figcaption>
</figure>
<p>The train program renders only a single plot: <code>log-likelihood-ascent.png</code>. In this particular demonstration we’re fitting to the complete dataset rather than holding out data, and the plot shows the log-likelihood of the model given the training data after each training epoch. Note that at 800 epochs there’s a sudden jump in performance. This is because when training a CB-CM, we first train an IP-CM for a certain number of iterations (in this case 800) before extending it to a CB-CM, which significantly reduces execution time.</p>
<p>To see a more complete analysis of the resulting model, we return to the <code>analyze</code> command. The results of our <code>train</code> command (i.e. <code>parameters.dat</code>) are saved in <code>experiments/{experiment}/{dataset}/discrete/com-based</code>, and so in this case we run</p>
<div class="sourceCode" id="cb19"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb19-1"><a href="#cb19-1" aria-hidden="true"></a><span class="ex">stack</span> exec -- analyze amir-awake-filtered cadetv1p438_tuning discrete com-based -c</span></code></pre></div>
<p>where the <code>-c</code> argument tells <code>train</code> that even though we’re analyzing a discrete model, the stimulus is continuous.</p>
<p>The outputs of the <code>analyze</code> command remain the same as for the synthetic experiment we considered previously, and so for the sake of brevity we’ll just look at the Fano factor and noise correlation scatter plots.</p>
<figure>
<img src="../../files/posts/papers/neural-mixtures/fano-factor-scatter-v1.gif" alt="fano-factor-scatter.gif"> <img src="../../files/posts/papers/neural-mixtures/noise-correlation-scatter-v1.gif" alt="noise-correlation-scatter.gif">
<figcaption class="float">
Scatter plots and <span class="math inline">\(r^2\)</span> of model vs. data statistics animated over stimuli.
</figcaption>
</figure>
<p>Based on these scatter plots the discrete CB-CM appears to do a good job at capturing the empirical statistics of the recorded V1 responses. To estimate whether the CB-CM captures the true statistics of the response distribution, we deploy the <code>cross-validation</code> command.</p>
<h4 id="cross-validating-conditional-mixtures">Cross-validating conditional mixtures</h4>
<p>The last program in the <code>neural-mixtures</code> CLI is <code>cross-validate</code>, which essentially wraps up the <code>train</code> program inside a cross-validation loop, and evaluates the desired model for a number of different mixture components <span class="math inline">\(d_K\)</span>. To cross-validate the performance of an IP-CM on our awake V1 data, we run</p>
<div class="sourceCode" id="cb20"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb20-1"><a href="#cb20-1" aria-hidden="true"></a><span class="ex">stack</span> exec -- cross-validate amir-awake-filtered cadetv1p438_tuning discrete com-based</span></code></pre></div>
<p>There are a number of parameters that we could set here (consult <code>stack exec -- cross-validate --help</code>), but the defaults are fine in this case. Note that where our <code>train</code> command from earlier took about 10 minutes on a 16-core machine, we are now running over 100 instances of this train command. As such, you may want to pass alternative (more conservative) parameters to <code>cross-validate</code> to get a sense of how it runs, and in any case please consult the following section on performance optimization on how to activate multi-threading in <code>neural-mixtures</code>. On a 16-core machine with multi-threading enabled, the above command took about 5 hours to complete.</p>
<code>cross-validate</code> generates a variety of plots, and below we look at two of them.
<figure>
<img src="../../files/posts/papers/neural-mixtures/ll-cross-validation-results.png" alt="ll-cross-validation-results.png"> <img src="../../files/posts/papers/neural-mixtures/pst-cross-validation-results.png" alt="pst-cross-validation-results.png">
<figcaption class="float">
Mean performance and standard error about the mean of model performance as a function of number of mixture components. Left: Log-likelihood of the data. Right: Log-posterior of the true stimulus given the response.
</figcaption>
</figure>
<p>For exact details on what this mean (especially the one on the right) please consult the paper, but in short they describe how well (Left) the model captures the statistics of how the recorded neurons respond to stimuli, and (Right) how well the model supports decoding the neural responses for the information they contain about the stimuli. As we see on the left, log-likelihood performance peaks around <span class="math inline">\(d_K = 25\)</span> mixture components, with most of the performance gain happening by about <span class="math inline">\(d_K = 15\)</span> components. On the right, we see that the log-posterior performance is less clearly peaked, but demonstrates a clear gain in performance up to around <span class="math inline">\(d_K = 15\)</span> components.</p>
<h3 id="additional-notes-on-working-with-the-neural-mixtures-library">Additional notes on working with the neural-mixtures library</h3>
<h4 id="multi-threading-and-performance-optimization">Multi-threading and performance optimization</h4>
<p>Every <code>neural-mixture</code> program can be given <code>+RTS -N{x}</code> as the last argument, which activates multi-threading. It is not part of <code>neural-mixtures</code> per se, but rather part of the Haskell runtime system (hence <code>+RTS</code>, and why it needs to be the last part of the command). Multi-threading confers no benefit to the <code>synthesize</code> and <code>analyze</code> programs. Multi-threading does not speed up the <code>train</code> program when fitting <code>poisson</code> models, but can significantly speed up the fitting of <code>com-based</code> models. The major use-case for multi-threading is the <code>cross-validation</code> program, because cross-validation is an “embarrassingly parallel” algorithm. When run in multi-threaded mode, <code>cross-validate</code> will create a thread for every fold and every mixture component, and so can easily populate a many-cored system. On my 16-core desktop, the <code>cross-validate</code> command we ran earlier took 5 hours to complete, as opposed to taking more than a day as it would in single-threaded mode.</p>
<p>The argument can be given simply as <code>+RTS -N</code>, which tells the program to run with as many threads as is available on the machine. This is often suboptimal, as it’s usually best to just create one thread per CPU core. On my computer with an AMD Ryzen 9 5950x (16 cores and 32 threads), running the <code>train</code> command from earlier takes about 6 minutes when run with <code>+RTS -N16</code>, about 9 minutes <code>+RTS -N</code>, and about 21 minutes when single-threaded.</p>
<p>Finally, when using <code>openblas</code>, you may want to adjust the <code>OMP_NUM_THREADS</code> environment variable. This tells <code>openblas</code> how many threads it’s allowed to use, and although it can improve performance when running single-threaded programs, in my experience it’s sometimes more efficient to e.g. set <code>OMP_NUM_THREADS=1</code> and allow the <code>neural-mixtures</code> to manage multi-threading exclusively.</p>
<h4 id="performance-scaling-and-computational-complexity">Performance scaling and computational complexity</h4>
<p>Theoretically, conditional mixture models scale quite well, and I have successfully applied the <code>neural-mixtures</code> CLI to synthetic data generated from thousands of neurons. The largest computation in the optimization is an outer product computation which is <span class="math inline">\(\mathcal O(d_K d_N)\)</span>, where <span class="math inline">\(d_K\)</span> is the number of mixture components and <span class="math inline">\(d_N\)</span> is the number of neurons. As such, computational complexity can grow more-or-less linearly with population size if <span class="math inline">\(d_K\)</span> is held fixed. That being said, sample complexity of the model also increases, and thus larger <span class="math inline">\(d_N\)</span> necessitates more training, and more data for a good fit.</p>
<h4 id="modifying-the-plots-and-code">Modifying the plots and code</h4>
<p>The data for all plots in <code>neural-mixtures</code> are first saved as <code>csv</code> files, and then rendered with <code>gnuplot</code>. The <code>gnuplot</code> scripts are available in the <code>plot-files</code> directory, and can be edited as desired. Also note that whenever <code>neural-mixtures</code> runs <code>gnuplot</code>, it outputs the command it runs in the terminal, and with some copy-pasting this command can be re-run to regenerate plots without regenerating the <code>csv</code>s. Alternatively, the <code>csv</code>s can be fed into alternative plotting programs, or used as the basis for further analyses.</p>
<p>If you want to take things further, the four command line programs are defined in the <code>executables</code> folder, and are quite high-level; the lower-level code is in the <code>libraries</code> folder, and the whole project is built on my so-called Geometric Optimization Libraries (Goal), which are available <a href="https://gitlab.com/sacha-sokoloski/goal">here</a>. Simple modifications of the executables should be possible without much knowledge of Haskell. If you are familiar with Haskell, then I encourage you to hack away, and I’m happy to support outside contributions to the codebase. Finally, if you aren’t familiar with Haskell, but wish you could access some of the <code>neural-mixtures</code> libraries more directly, feel free to contact me and I’d be willing to put bindings together for e.g. Python.</p>
<h3 class="unnumbered" id="bibliography">Bibliography</h3>
<div id="refs" class="references hanging-indent" role="doc-bibliography">
<div id="ref-kanitscheider_measuring_2015">
<p>Kanitscheider, Ingmar, Ruben Coen-Cagli, Adam Kohn, and Alexandre Pouget. 2015. “Measuring Fisher Information Accurately in Correlated Neural Populations.” <em>PLoS Computational Biology</em> 11 (6): e1004218. <a href="http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004218">http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004218</a>.</p>
</div>
<div id="ref-sokoloski_modelling_2021">
<p>Sokoloski, Sacha, Amir Aschner, and Ruben Coen-Cagli. 2021. “Modelling the Neural Code in Large Populations of Correlated Neurons.” Edited by Jonathan W Pillow, Joshua I Gold, and Kenneth D Harris. <em>eLife</em> 10 (October): e64615. <a href="https://doi.org/10.7554/eLife.64615">https://doi.org/10.7554/eLife.64615</a>.</p>
</div>
</div>
    </section>
</article>

        </main>

        <footer>
            <p>email: <a href="mailto:sacha.sokoloski@mailbox.org">sacha.sokoloski@mailbox.org</a></p>
            <p>gitlab: <a href="https://gitlab.com/sacha-sokoloski">gitlab.com/sacha-sokoloski</a> </p>
            <p>Site generated by <a href="http://jaspervdj.be/hakyll">Hakyll</a></p>
        </footer>
    </body>
</html>
