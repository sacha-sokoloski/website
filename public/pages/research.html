<!doctype html>

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js">
</script>


<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sacha Sokoloski - Research</title>
        <link rel="stylesheet" href="../css/default.css" />
        <link rel="stylesheet" href="../css/syntax.css" />
        <link rel="stylesheet" href="../css/float.css" />
    </head>
    <body>
        <header>
            <div class="title">
                <a href="../">Sacha Sokoloski - Research</a>
            </div>
            <nav>
                <a href="../">Home</a>
                <a href="../pages/research.html">Research</a>
                <a href="../pages/blog.html">Blog</a>
                <a href="../pages/personal.html">Personal</a>
                <a href="../pages/cv.html">CV</a>
            </nav>
        </header>

        <main role="main">
            <style>

figure.code {
    float: right;
    margin-top: 20px;
    max-width: 180px;
    min-width: 180px;
    background-color: #232629;


@media (max-width: 559px) {

    img.hgm {
        height: auto;
        width: 260px;
        margin: auto;
        padding: 6px;
    }

    img.log-likelihood {
        width: 95%;
        height: auto;
        padding: 2.5%;
    }

    a.kant {
        height: auto;
        width: 200px;
        margin: auto;
    }

    img.kant {
        height: auto;
        width: 220px;
    }

}


}

@media (min-width: 560px) {

    figure.brain {
        width: 39%;
        margin-top: 5px;
    }

    figure.hgm {
        margin-top: 20px;
        width: 34%;
    }

    figure.log-likelihood {
        width: 48%;
        margin-top: 8px;
    }

    figure.kant {
        width: 28%;
        margin-top: 12px;
        margin-bottom: 6px;
    }

}

</style>
<p><em>(NB: This page needs a round of updating, though remains relevant. I still like Kant).</em></p>
<figure class="brain float left-float">
<a title="Selket [CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0/)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Ventral-dorsal_streams.svg"> <img class="brain float left-float" alt="Ventral-dorsal streams" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Ventral-dorsal_streams.svg/512px-Ventral-dorsal_streams.svg.png"></a>
<figcaption class="float">
Neurons in visual cortex (blue) compute simple features of the visual input. Their output is sent along the dorsal (green) and ventral (purple) streams to compute object location and identity.
</figcaption>
</figure>
<h3 id="towards-a-theory-of-neural-computation">Towards a Theory of Neural Computation</h3>
<p>I aim to understand how populations of neurons implement elementary computations, and how these computations are combined to support complex cognition. Towards this aim, I develop theories of how the brain infers the state of the world and refines its inferences through experience, in a manner that is consistent with the distributed, recurrent, and hierarchical nature of neural computation. In order to validate theories of the brain, I develop statistical models that capture hidden structure in noisy neural recordings, and computer libraries capable of simulating and fitting such models efficiently and without error. In the following paragraphs I will sketch the past, present, and future of my work in these areas.</p>
<figure class="hgm float right-float">
<img class="hgm float right-float" src="../files/random-images/hgm.png" alt="Hierarchical Model of the World">
<figcaption class="float">
The brain as a hierarchical graphical model: the bottom of the hierarchy (Z) are sensations over time (t). Higher-level properties and concepts (Y and X) are dynamically inferred by the brain based on the sensations.
</figcaption>
</figure>
<h3 id="the-bayesian-brain">The Bayesian Brain</h3>
<p>I take the view that the brain encodes a recurrent, hierarchical model of the world <span class="citation" data-cites="friston_hierarchical_2008 pitkow_inference_2017">(Friston <a href="#ref-friston_hierarchical_2008" role="doc-biblioref">2008</a>; Pitkow and Angelaki <a href="#ref-pitkow_inference_2017" role="doc-biblioref">2017</a>)</span>. In this view, sensory receptors (e.g. the retina) constitute the bottom of the hierarchy, and the brain infers encodings of higher-level properties and concepts given the dynamic activity of its senses. Bayesian inference is the mathematically rigorous formulation of this inference problem <span class="citation" data-cites="pouget_probabilistic_2013">(Pouget et al. <a href="#ref-pouget_probabilistic_2013" role="doc-biblioref">2013</a>)</span>, but solving Bayesian inference problems is rarely trivial.</p>
<p>In <span class="citation" data-cites="sokoloski_implementing_2017">Sokoloski (<a href="#ref-sokoloski_implementing_2017" role="doc-biblioref">2017</a>)</span> I showed how this problem can be solved exactly for a simple form of theoretical, dynamic neural population when the parameters of the neural population satisfy certain constraints. The results in this paper were a subset of the work I did in my dissertation <span class="citation" data-cites="sokoloski_implementing_2019">(Sokoloski <a href="#ref-sokoloski_implementing_2019" role="doc-biblioref">2019</a>)</span>, wherein I analyzed the general conditions under which dynamic Bayesian inference is computationally tractable.</p>
<p>Ultimately, animals infer the state of the world in order to behave effectively, and in my master’s thesis I did preliminary work on formulating motor control as a tractable inference problem <span class="citation" data-cites="sokoloski_efficient_2013">(Sokoloski <a href="#ref-sokoloski_efficient_2013" role="doc-biblioref">2013</a>)</span>. As a postdoctoral researcher in the lab of <a href="https://summer.haskell.org/news/2018-09-01-final-results.html">Philipp Berens</a>, I am combining my experience in control theory and neural modelling to understand how reinforcement learning drives cellular diversity and the functional architecture of the retina.</p>
<h3 id="neural-and-machine-learning">Neural and Machine Learning</h3>
<p>Bayesian inference combines information from observations with prior knowledge. In the Bayesian framework, the question of how the brain learns reduces to how the brain refines its prior knowledge given its history of sensory experience <span class="citation" data-cites="berkes_spontaneous_2011">(Berkes et al. <a href="#ref-berkes_spontaneous_2011" role="doc-biblioref">2011</a>)</span>. This reduction thus unifies the problem of how the brain learns with the problem of how to fit models to neural data; both problems can be solved by the method of maximum likelihood.</p>
<figure class="log-likelihood float left-float">
<img class="log-likelihood float left-float" src="../files/random-images/log-likelihood-ascent.png" alt="Novel Training Algorithms">
<figcaption class="float">
Stochastic expectation-maximization (SEM) and stochastic gradient ascent (SGA) can fit models of correlated neural activity. With a bit of math (Hybrid) we may exceed them and achieve near optimal performance <span class="citation" data-cites="sokoloski_conditional_2019">(Sokoloski and Coen-Cagli <a href="#ref-sokoloski_conditional_2019" role="doc-biblioref">2019</a>)</span>.
</figcaption>
</figure>
<p>In the lab of <a href="https://sites.google.com/site/rubencoencagli/home">Ruben Coen-Cagli</a> I developed a novel model of correlated, context-dependent neural population activity, and an efficient algorithm for fitting it to data <span class="citation" data-cites="sokoloski_modelling_2021">(Sokoloski, Aschner, and Coen-Cagli <a href="#ref-sokoloski_modelling_2021" role="doc-biblioref">2021</a>)</span>. Neural correlations can have a profound effect on neural computation <span class="citation" data-cites="kohn_correlations_2016">(Kohn et al. <a href="#ref-kohn_correlations_2016" role="doc-biblioref">2016</a>)</span>, and one of my ongoing projects is to understand if the correlations amongst neurons in primary visual cortex might facilitate tractable Bayesian inference.</p>
<p>In my dissertation <span class="citation" data-cites="sokoloski_implementing_2019">(Sokoloski <a href="#ref-sokoloski_implementing_2019" role="doc-biblioref">2019</a>)</span> I did preliminary work on a novel algorithm for fitting hierarchical models, which remains an open problem in machine learning and computational neuroscience <span class="citation" data-cites="lecun_deep_2015">(LeCun, Bengio, and Hinton <a href="#ref-lecun_deep_2015" role="doc-biblioref">2015</a>)</span>. This algorithm relies on the aforementioned constraints for tractable Bayesian inference, and the work I have so far described constitute steps towards implementing it. If I am successful in realizing this larger project, then I hope to demonstrate how the organization of tuning and correlations in neural populations facilitate both tractable inference and learning in the brain.</p>
<figure class="right-float float code">
<div class="sourceCode" id="cb1"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true"></a> (<span class="op">&lt;.&gt;</span>)</span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true"></a><span class="ot">   ::</span> c <span class="op">#</span> x</span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true"></a>   <span class="ot">-&gt;</span> c <span class="op">#*</span> x</span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true"></a>   <span class="ot">-&gt;</span> <span class="dt">Double</span></span></code></pre></div>
<figcaption class="float">
Dot product of two points in Goal. The first point is in <strong>c</strong> coordinates on the <strong>x</strong> manifold, and the second is in the dual space of the first.
</figcaption>
</figure>
<h3 id="type-safe-numerical-optimization">Type-Safe Numerical Optimization</h3>
<p>In attempting to implement, apply, and simulate complex models, one comes up against human limitations in our ability to write error-free machine code. Strongly-typed languages assist us in avoiding such errors by granting the compiler the ability to verify our code before we run it. At the same time, one cannot embed all code at the type-level, and libraries written in strongly-typed languages must tradeoff verifiability with practicality.</p>
<p>I have developed a set of libraries for the programming language <a href="https://www.haskell.org/">Haskell</a> which I call the Geometric Optimization Libraries (Goal). Goal provides types and functions based on simple ideas from differential and information geometry <span class="citation" data-cites="amari_methods_2007">(Amari and Nagaoka <a href="#ref-amari_methods_2007" role="doc-biblioref">2007</a>)</span>. Essentially, Goal distinguishes vectors of numbers as points on a manifold in a given coordinate system. Many fundamental mathematical operations can be formulated as changes in coordinates, and Goal thereby provides a compact yet general interface to optimization and statistics. Moreover, by embedding these fundamental concepts at the type-level, Goal allows Haskell type-checkers to serve as simple proof assistants, and in this way Goal facilitates my mathematical work as well.</p>
<figure class="kant float left-float">
<a class="kant float left-float" title="Stéphane Lemarchand Caricaturiste [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Caricature_Kant_2007.jpg"> <img class="kant float left-float" alt="Caricature Kant 2007" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Caricature_Kant_2007.jpg/256px-Caricature_Kant_2007.jpg"></a>
<figcaption class="float">
Immanuel Kant showed how we may arrive at truths about reality through reason alone.
</figcaption>
</figure>
<h3 id="embodiment-enaction-and-transcendental-arguments">Embodiment, Enaction, and Transcendental Arguments</h3>
<p>I believe that data cannot speak for itself, and must be interpeted through the lens of well-motivated theory. Physicists have produced insights in many fields by applying their theories and physical intuition to revealing simple structures in data. Nevertheless, a purely physicalist view of the brain ignores what our faculty of introspection and our innate intuitions as cognitive beings reveal about the structure of cognition and neural activity. In particular, I believe that theories of the brain as a Bayesian inference machine are underconstrained <span class="citation" data-cites="bowers_bayesian_2012 colombo_bayes_2012">(Bowers and Davis <a href="#ref-bowers_bayesian_2012" role="doc-biblioref">2012</a>; Colombo and Seriès <a href="#ref-colombo_bayes_2012" role="doc-biblioref">2012</a>)</span>, and that we may at least partially address this with <em>a priori</em> analyses of the embodied and enactive nature of cognition <span class="citation" data-cites="thompson_mind_2007 stewart_enaction_2011">(Thompson <a href="#ref-thompson_mind_2007" role="doc-biblioref">2007</a>; Stewart, Gapenne, and Paolo <a href="#ref-stewart_enaction_2011" role="doc-biblioref">2011</a>)</span>.</p>
<p>Once upon a time I was a student of philosophy. Although I did not pursue a career in the subject, philosophy continues to inform my research. I dream of one day teaching introductory philosophy courses, and perhaps even contributing to the field in my winter years.</p>
<p><br></p>
<h1 class="unnumbered" id="bibliography">Bibliography</h1>
<div id="refs" class="references hanging-indent" role="doc-bibliography">
<div id="ref-amari_methods_2007">
<p>Amari, Shun-ichi, and Hiroshi Nagaoka. 2007. <em>Methods of Information Geometry</em>. Vol. 191. American Mathematical Soc. <a href="http://books.google.com/books?hl=en&amp;lr=&amp;id=vc2FWSo7wLUC&amp;oi=fnd&amp;pg=PR7&amp;dq=amari+methods&amp;ots=4GnrDz3cHZ&amp;sig=wcvH9syYELbsfXeTSxilTcz74GQ">http://books.google.com/books?hl=en&amp;lr=&amp;id=vc2FWSo7wLUC&amp;oi=fnd&amp;pg=PR7&amp;dq=amari+methods&amp;ots=4GnrDz3cHZ&amp;sig=wcvH9syYELbsfXeTSxilTcz74GQ</a>.</p>
</div>
<div id="ref-berkes_spontaneous_2011">
<p>Berkes, Pietro, Gergő Orbán, Máté Lengyel, and József Fiser. 2011. “Spontaneous Cortical Activity Reveals Hallmarks of an Optimal Internal Model of the Environment.” <em>Science</em> 331 (6013): 83–87. <a href="https://doi.org/10.1126/science.1195870">https://doi.org/10.1126/science.1195870</a>.</p>
</div>
<div id="ref-bowers_bayesian_2012">
<p>Bowers, Jeffrey S., and Colin J. Davis. 2012. “Bayesian Just-so Stories in Psychology and Neuroscience.” <em>Psychological Bulletin</em> 138 (3): 389–414. <a href="https://doi.org/10.1037/a0026450">https://doi.org/10.1037/a0026450</a>.</p>
</div>
<div id="ref-colombo_bayes_2012">
<p>Colombo, Matteo, and Peggy Seriès. 2012. “Bayes in the Brain—on Bayesian Modelling in Neuroscience.” <em>The British Journal for the Philosophy of Science</em> 63 (3): 697–723. <a href="http://bjps.oxfordjournals.org/content/63/3/697.short">http://bjps.oxfordjournals.org/content/63/3/697.short</a>.</p>
</div>
<div id="ref-friston_hierarchical_2008">
<p>Friston, Karl. 2008. “Hierarchical Models in the Brain.” Edited by Olaf Sporns. <em>PLoS Computational Biology</em> 4 (11): e1000211. <a href="https://doi.org/10.1371/journal.pcbi.1000211">https://doi.org/10.1371/journal.pcbi.1000211</a>.</p>
</div>
<div id="ref-kohn_correlations_2016">
<p>Kohn, Adam, Ruben Coen-Cagli, Ingmar Kanitscheider, and Alexandre Pouget. 2016. “Correlations and Neuronal Population Information.” <em>Annual Review of Neuroscience</em> 39 (1): 237–56. <a href="https://doi.org/10.1146/annurev-neuro-070815-013851">https://doi.org/10.1146/annurev-neuro-070815-013851</a>.</p>
</div>
<div id="ref-lecun_deep_2015">
<p>LeCun, Yann, Yoshua Bengio, and Geoffrey Hinton. 2015. “Deep Learning.” <em>Nature</em> 521 (7553): 436–44. <a href="http://www.nature.com/nature/journal/v521/n7553/abs/nature14539.html">http://www.nature.com/nature/journal/v521/n7553/abs/nature14539.html</a>.</p>
</div>
<div id="ref-pitkow_inference_2017">
<p>Pitkow, Xaq, and Dora E. Angelaki. 2017. “Inference in the Brain: Statistics Flowing in Redundant Population Codes.” <em>Neuron</em> 94 (5): 943–53. <a href="https://doi.org/10.1016/j.neuron.2017.05.028">https://doi.org/10.1016/j.neuron.2017.05.028</a>.</p>
</div>
<div id="ref-pouget_probabilistic_2013">
<p>Pouget, Alexandre, Jeff Beck, Wei Ji Ma, and Peter Latham. 2013. “Probabilistic Brains: Knowns and Unknowns.” <em>Nature Neuroscience</em> 16 (9): 1170–8. <a href="http://www.nature.com/neuro/journal/v16/n9/abs/nn.3495.html">http://www.nature.com/neuro/journal/v16/n9/abs/nn.3495.html</a>.</p>
</div>
<div id="ref-sokoloski_efficient_2013">
<p>Sokoloski, Sacha. 2013. “Efficient Stochastic Control with Kullback Leibler Costs Using Kernel Methods.” Master’s thesis, Technical University of Berlin. <a href="https://www.ki.tu-berlin.de//fileadmin/fg135/publikationen/Sokoloski_2013_ESC.pdf">https://www.ki.tu-berlin.de//fileadmin/fg135/publikationen/Sokoloski_2013_ESC.pdf</a>.</p>
</div>
<div id="ref-sokoloski_implementing_2019">
<p>———. 2019. “Implementing Bayesian Inference with Neural Networks.” Dissertation, University of Leipzig. <a href="http://nbn-resolving.de/urn:nbn:de:bsz:15-qucosa2-347034">http://nbn-resolving.de/urn:nbn:de:bsz:15-qucosa2-347034</a>.</p>
</div>
<div id="ref-sokoloski_implementing_2017">
<p>———. 2017. “Implementing a Bayes Filter in a Neural Circuit: The Case of Unknown Stimulus Dynamics.” <em>Neural Computation</em> 29 (9): 2450–90. <a href="https://doi.org/10.1162/neco_a_00991">https://doi.org/10.1162/neco_a_00991</a>.</p>
</div>
<div id="ref-sokoloski_modelling_2021">
<p>Sokoloski, Sacha, Amir Aschner, and Ruben Coen-Cagli. 2021. “Modelling the Neural Code in Large Populations of Correlated Neurons.” Edited by Jonathan W Pillow, Joshua I Gold, and Kenneth D Harris. <em>eLife</em> 10 (October): e64615. <a href="https://doi.org/10.7554/eLife.64615">https://doi.org/10.7554/eLife.64615</a>.</p>
</div>
<div id="ref-sokoloski_conditional_2019">
<p>Sokoloski, Sacha, and Ruben Coen-Cagli. 2019. “Conditional Finite Mixtures of Poisson Distributions for Context-Dependent Neural Correlations.” <em>arXiv:1908.00637 [Cs, Stat]</em>, August. <a href="http://arxiv.org/abs/1908.00637">http://arxiv.org/abs/1908.00637</a>.</p>
</div>
<div id="ref-stewart_enaction_2011">
<p>Stewart, John Robert, Olivier Gapenne, and Ezequiel A. Di Paolo. 2011. <em>Enaction: Toward a New Paradigm for Cognitive Science</em>. MIT Press.</p>
</div>
<div id="ref-thompson_mind_2007">
<p>Thompson, Evan. 2007. <em>Mind in Life: Biology, Phenomenology, and the Sciences of Mind</em>. Harvard University Press.</p>
</div>
</div>
        </main>

        <footer>
            <p>email: <a href="mailto:sacha.sokoloski@mailbox.org">sacha.sokoloski@mailbox.org</a></p>
            <p>gitlab: <a href="https://gitlab.com/sacha-sokoloski">gitlab.com/sacha-sokoloski</a> </p>
            <p>Site generated by <a href="http://jaspervdj.be/hakyll">Hakyll</a></p>
        </footer>
    </body>
</html>
